---
title:
toc: false
nosearch: true
norss: true
showDate: false
sharemenu: false
---

{{<slogan content="Il piacere del sapere"
>}}

# La volpe vi dà il benvenuto

Mi chiamo Alessandra e ho creato questo blog per mettere a disposizione le mie ricerche accademiche (e non), perché a che serve fare ricerca se poi il mondo non può beneficiarne? 

Gli articoli di questo blog sono di taglio accademico e affrontano i temi della letteratura secondo una prospettiva interdisciplinare e comparatistica che li accosta agli altri campi del sapere, come l'arte, il cinema, gli studi di genere e così via.

Per ricevere aggiornamenti sui nuovi articoli clicca sull'icona **RSS** e aggiungi Volupia al tuo feed, oppure seguila su <a href="https://www.instagram.com/volupia_arufox/">Instagram</a>!

---

{{< figure
    alt="scaffale di libri"
    src="/img/home/libri.jpg"
    attr="Foto di <a href='https://unsplash.com/it/@thkelley' target='_blank'>Thomas Kelley</a>"
	class="onlydark"
>}}

{{< figure
    alt="tanti libri"
    src="/img/home/pagine.jpg"
    attr="Foto di <a href='https://unsplash.com/@impatrickt' target='_blank'>Patrick Tomasso</a>"
	class="onlylight"
>}}
  
<p style="padding-top: 40px">Questo blog non rappresenta una testata giornalistica in quanto viene aggiornato senza alcuna periodicità. Esso non può pertanto considerarsi un prodotto editoriale ai sensi della legge n. 62 del 07/03/2001.</p>

<script data-name="BMC-Widget" data-cfasync="false" src="https://cdnjs.buymeacoffee.com/1.0.0/widget.prod.min.js" data-id="arufox" data-description="Support me on Buy me a coffee!" data-message="Se ti piace quel che scrivo e vuoi supportarmi nel mio lavoro, offrimi una pizza! È facile e non richiede un account." data-color="#FF813F" data-position="Right" data-x_margin="90" data-y_margin="18"></script>