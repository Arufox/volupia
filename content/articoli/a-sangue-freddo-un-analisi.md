--- 
title: "A sangue freddo. Un'analisi"
date: 2022-04-06T16:58:32+02:00
featured: false
description: "A sangue freddo è un romanzo del 1966 scritto da Truman Capote. Tratto da una storia vera, è il resoconto dettagliato del quadruplice omicidio della famiglia Clutter, ma è sia un'impietosa radiografia dell'American Dream vissuto in provincia, sia un'inchiesta sul sistema giudiziario americano."
tags:
- truman capote
- in cold blood
- giornalismo
- letteratura americana
- realismo
- romanzo giallo
- analisi
toc: true
image: "/img/articoli/crime.jpg"
imageCaption: "Foto di <a href='https://unsplash.com/@nampoh' target='_blank'>Maxim Hopman</a>"
draft: false
---

---

_In Cold Blood_ è un romanzo dello scrittore statunitense Truman Capote, pubblicato nel 1966 con Random House dopo essere uscito in puntate sul "New Yorker" tra il settembre e l'ottobre del 1965. Tratto da una storia vera, è il resoconto dettagliato del quadruplice omicidio della famiglia Clutter, ma è anche sia un'impietosa radiografia dell'_American Dream_ vissuto in provincia, sia un'inchiesta sul sistema giudiziario americano.

## Una _journalistic novel_

L'obiettivo di Capote era sì di raccontare i fatti davvero accaduti, ma adottando gli espedienti narrativi tipici del romanzo di finzione, esperimento che Capote definì "nonfiction novel". Si tratta difatti di un genere giornalistico, la _journalistic novel_, perfettamente intrecciato con la letteratura: l'opera viene letta come se fosse un romanzo, ma si tratta dell'elaborazione estrema di un fatto di cronaca. La particolarità di questo genere letterario (all'epoca una novità) è che la trama si sviluppa attorno ai singoli avvenimenti generatisi attorno a una vicenda, il fatto di cronaca, appunto.

Il romanzo di Capote è una sapiente ricostruzione _a posteriori_ ed è frutto di un lungo e paziente lavoro di raccolta di **documenti** e **registrazioni ufficiali**. L'autore era dell'idea che per raccontare un fatto attraverso i pensieri, le parole e le emozioni di chi lo aveva vissuto in prima persona fosse necessario passare giorni, talvolta mesi, proprio con quelle persone. L'esposizione puntuale delle dinamiche della vicenda è stata dunque ottenuta grazie all'assidua frequentazione dei colpevoli, racconti che sono stati poi filtrati dall'originale rielaborazione stilistica di Capote. Il risultato finale è talmente buono che nei personaggi è persino percepibile l'accento e l'intonazione della voce. Il tutto genera in chi legge l'impressione di trovarsi di fronte alla pura verità, alla **realtà nuda e cruda**, proprio come quella che può emergere dalle carte giudiziarie o dalla vita stessa. È questo l'ambito del _New Journalism_, il quale non si limita soltanto al visibile (i fatti), ma si propone di perforare quelle che sono le sfere dell'invisibile e dell'immateriale (emozioni, passioni, moventi, ecc...).

## La tecnica narrativa

La tecnica narrativa utilizzata è l'_entrelacement_, che consiste nel raccontare contemporaneamente storie diverse che alla fine andranno a congiungersi nella trama principale. Utilizzata anche da Omero e Ariosto, la caratteristica principale di tale tecnica è di avere una narrazione continuamente sospesa, con buchi narrativi che verranno colmati man mano che si conosce la storia degli altri personaggi. La trama principale si sviluppa dunque attorno a diverse storie legate tra loro che avvengono contemporaneamente, dando a chi legge la sensazione che i fatti avvengano "intorno a sé", in un clima di continua _suspense_.

Il ritmo del romanzo è lento e molto cinematografico, fatto di lunghe panoramiche, lenti avvicinamenti ai particolari e un grande accumulo di informazioni. Invece di annunciare il fatto nei suoi elementi essenziali, Capote gioca con i **dettagli** (ad esempio, nella scena del ritrovamento delle vittime, la descrizione si sofferma sui cavi recisi del telefono e sul borsellino di Nancy sul pavimento), creando una suggestione iniziale e un effetto romanzesco. Il narratore non si dichiara direttamente e scrive in terza persona. Celandosi dietro agli eventi, ne anticipa le conseguenze e assume più punti di vista interni, ovvero quelli di personaggi e testimoni. Più che di narratore onnisciente, si potrebbe parlare piuttosto di un **narratore onnipresente**.

Il realismo descrittivo di Capote registra ambienti, stili e mode limitandosi soltanto alla **realtà dei fatti**: per evitare di esprimere un qualunque giudizio, l'autore non si serve infatti di descrizioni narrative, bensì delinea il carattere dei personaggi per mezzo delle loro azioni e delle loro parole. Persino i tratti più profondi della loro psiche sono resi da documentazioni ufficiali, come ad esempio i referti psichiatrici degli assassini Dick e Perry. Inoltre, il romanzo contiene al suo interno dei _feature articles_, cioè articoli veri e propri che hanno lo scopo di permettere a chi legge di scavare nella storia per tirarne fuori ciò di cui i fatti si nutrono, dunque passioni, emozioni, atteggiamenti, pregiudizi e così via. Il tutto viene narrato con sequenze dall'andatura cinematografica, con vicende affollate di personaggi e ricche di descrizioni.

Un simile stile narrativo coincide difatti con l'avvento dell'informazione televisiva, la quale aveva creato un pubblico abituato a _vedere_ le notizie grazie all'affermazione delle nuove tecniche di registrazione della realtà. Inoltre, la ricerca di verità aveva prodotto nel pubblico una ricerca continua dell'**effetto di realtà**. Ciononostante, all'uscita del romanzo Capote venne accusato di voyeurismo cinico: il pubblico sembrava non aver apprezzato quest'attenzione minuziosa nei confronti di un fatto di cronaca nera. Se è vero che Capote ci mostra lo sterminio brutale di una famiglia per mano di due psicopatici, è anche vero che, nonostante la descrizione spietata delle dinamiche della violenza, le vittime vengono rappresentate e descritte in modo pietoso e rispettoso.

## Struttura dell'opera

Il romanzo è suddiviso in quattro parti: _Gli ultimi a vederli vivi_, _Persone sconosciute_, _Risposta_ e _L'angolo_.

### _Gli ultimi a vederli vivi_

La prima parte ha il compito di esporre il contesto in cui matura il fatto, cioè l'omicidio della famiglia Clutter. Come una sorta di montaggio incrociato, nella narrazione si susseguono brani dedicati alla famiglia Clutter, ai detective e agli autori del delitto.

Capote è magistrale nella descrizione dei personaggi nel loro ambiente e del modo in cui essi stanno nel mondo; inoltre è famoso per le storie gotiche sospese tra realtà e fantasia e legate alla tradizione meridionale statunitense. La vicenda si svolge così nel cuore degli Stati Uniti d'America, negli stati del Midwest, la terra dei pionieri delle praterie. In gran parte paesi agricoli e industriali, sono considerati il centro dei valori morali (e degli stereotipi) americani, come il duro lavoro, la casa, la famiglia e la religione. Tali elementi sono tutti rintracciabili nel testo, dal quale apprendiamo che la piccola cittadina di Holcomb, in Kansas, è una comunità socialmente e moralmente omogenea.

Fondata originariamente dai pionieri delle praterie, a **Holcomb** c'è un forte senso di comunità, i cui abitanti sono riuniti in clan, cioè gruppi di persone unite da una parentela definita dalla discendenza da un antenato comune. Il **signor Clutter** può essere paragonato al padron 'Ntoni della nostra letteratura: proprietario terriero e agricoltore indipendente, era il capofamiglia (_"le sue leggi erano le sue leggi"_). Aveva fondato la propria famiglia sui valori del lavoro, dell'amore per la comunità e della religione Metodista; la sua cerchia di amici era limitata alla comunità della Prima Chiesa Metodista di Garden City e non frequentava gente che bevesse o fumasse; non dormiva nella stessa camera da letto della moglie e aveva vietato alla figlia Nancy di sposare il fidanzato Bobby perché cattolico.

**Nancy Clutter** impersona invece l'ideale femminile dell'autore, una ragazza che _"ha sempre fretta ma ha sempre tempo"_. La sua personalità emerge dalla descrizione della sua cameretta: una ragazza di 16 anni semplice, buona, un po' civetta ma innamorata del fidanzato.

In questa prima parte del romanzo il prima e il dopo dell'omicidio sono narrati con molta precisione, mentre lo svolgimento del delitto stesso rimane un mistero. Questo vuoto narrativo struttura difatti la trama dell'intero romanzo, il quale è incentrato sulla ricerca del movente e della dinamica della strage. L'autore non dice nemmeno che i colpevoli sono Dick e Perry, ma chi legge può intuirlo dalla descrizione della scena a pagina 92, quando Dick rincasa e mette a mollo gli stivali, _"tinteggiando l'acqua di rosa"_. Capote descrive allora lo sgomento di una piccola comunità alle prese con una sofferta notorietà e sconvolta dalla paura di un "nemico interno". Tale terrore si manifesta in modo irrazionale, inspiegabile e assurdo persino nelle persone più miti: siamo di fronte all'ossessione americana per il **male assoluto**.

### _Persone sconosciute_

In questa seconda parte, con un montaggio incrociato, vengono presentate le figure dei due assassini e dei detective, tra le quali spicca il personaggio del **detective Dewey**, l'eroe talmente tormentato dalla ricerca delle cause del male da vedere compromesso l'equilibrio normale della propria esistenza. Egli può essere assunto quale punto di vista privilegiato della narrazione.

**Perry Smith** è uno degli assassini e viene presentato sia in modo diretto (ad esempio, attraverso i suoi spostamenti dopo l'omicidio), sia in modo indiretto (cioè attraverso i documenti che vengono inseriti nel testo, le lettere di amici e parenti). Per metà Cherokee, Perry è la malinconica figura del meticcio e rappresenta la grande e complessa sfera del **disagio americano**, di coloro che faticano a integrarsi perché diversi. Attraverso questo personaggio Capote ci racconta quella parte dell'America che "non doveva essere narrata". Si tratta di una tematica sociale strettamente connessa al clima del periodo in cui l'opera è stata scritta, gli anni Sessanta, gli anni della contestazione della cultura ufficiale o _Mainstream_, della denuncia della enorme fascia di disagio presente nella società americana, delle lotte per i diritti civili dei neri, dei poveri e delle minoranze in generale. Nel testo troviamo atteggiamenti discriminatori e razzisti nella figura di **Dick**, il compagno di Perry, descritto come la tipica bellezza americana, che più volte afferma che la vita di un nero non vale nulla. Paradossalmente Perry cerca sempre l'approvazione e la stima del compagno razzista, raccontando persino la bugia di aver ucciso un nero per divertimento.

### _Risposta_

La terza parte fornisce finalmente le risposte ai due grandi enigmi del romanzo: il movente e la dinamica del delitto. Tuttavia, grazia alla figura del detective Dewey, il lettore è spinto a interrogarsi sulle ragioni più profonde della violenza umana: com'è possibile che ci siano persone in grado di perdere tutta la propria umanità davanti a un altro essere umano? L'idea di fondo è che l'_Homo sapiens_ sia una creatura fragile e, consapevole della propria fragilità, aggredisce l'ambiente e le altre specie per difendersi e autoconservarsi. L'etologia umana chiama pseudospeciazione quel processo tramite il quale gli esseri umani si organizzano in gruppi più o meno omogenei in base a tratti culturali in comune (lingua, religione, usi, costumi ecc...). Questo comportamento tende a distinguere nell'umanità diverse "specie" o persino "razze" e porta alcuni soggetti a essere molto aggressivi nei confronti di chi è diverso dal gruppo.

In questa parte del romanzo assistiamo all'arresto dei due assassini, davanti allo sbigottimento generale nello scoprire che quelli che venivano chiamati "mostri" o "bestie" avevano in realtà _"forma umana"_. La scena dell'arresto è molto teatrale ed è resa più forte dalla caduta della prima neve dell'anno, simbolo non solo di morte (per i due criminali) ma anche di purificazione universale. Scopriamo allora che la strage dei Clutter non aveva in realtà **alcun movente**: attirati dalla vaga informazione di una cassaforte nascosta in casa Clutter, i due malviventi inizialmente volevano soltanto commettere il furto; tra l'altro da parte dei Clutter non avevano riscontrato alcun tipo di resistenza perché collaborativi, ma è a causa di un attrito psicologico tra i due complici che scatta in loro la furia omicida.

I due criminali non sono certamente nuovi all'omicidio. Sono principalmente ladri ed è per rubare che uccidono. Dick considera il furto uno stile di vita e ruba per il piacere di sottrarre agli altri ciò che lui non ha. Perry invece considera l'omicidio una forma superiore di furto perché significa letteralmente sottrarre la vita all'altro.

### _L'angolo_

La quarta e ultima parte del romanzo narra la storia del processo e dell'esecuzione dei colpevoli. Ciononostante, assistiamo tuttavia a un ironico rovesciamento della metafora che regge l'intera narrazione e che dà il titolo all'opera. Al sangue freddo di Dick e Perry si contrappone la freddezza non meno spietata di una società che, regolata da leggi barbare, li condanna a morte. La scena del processo, ad esempio, fa del tribunale un campo di battaglia in cui si scontrano la legge e i principi del Cristianesimo: la Bibbia viene qui contestata in modo cavilloso nel suo Quinto Comandamento (_"non uccidere"_), il quale dà pieno diritto allo Stato di mettere a morte gli assassini.

Molto interessanti anche le pagine dedicate ai **referti psichiatrici** degli assassini, che costituiscono una sorta di verità alternativa che la morale comune non vuole e ha paura di accettare. Capote ci fa capire che in America la vita di un criminale dipende dall'esito psichiatrico positivo o negativo circa la capacità di intendere e volere, ma escludendone le perizie psicologiche. Ciò significa che, secondo il referto psichiatrico, **Dick** era sì in grado di distinguere il bene dal male, ma soffriva di un grave disturbo della personalità. Viene classificato come un individuo impulsivo che tende ad agire senza pensare alle conseguenze delle proprie azioni, che non apprende dall'esperienza e che scarica la frustrazione in comportamenti antisociali; inoltre è soggetto a frequenti perdite di coscienza e periodi di amnesia in seguito a una commozione cerebrale.

Su **Perry** invece lo psichiatra non riesce a farsi un'opinione sulla sua capacità di distinguere il bene dal male e mostra atteggiamenti patologici affini alla schizofrenia paranoica. Difatti mostra un atteggiamento paranoico nei confronti del mondo esterno: è sospettoso, diffida dagli altri, pensa di essere discriminato, incompreso e che alla fine verrà sempre tradito; non riesce a valutare le intenzioni e i sentimenti altrui e non riesce bene a distinguere la realtà dalle sue proiezioni mentali; è iracondo, disprezza l'umanità e pensa che le persone meritino qualsiasi cosa egli faccia loro. Vittima di abusi e violenze, da piccolo Perry era stato privato di tutto ciò che fa di un essere umano una persona (la famiglia, l'amore, la fede, l'istruzione), ciononostante riesce ancora a ritrovare la propria umanità davanti agli indifesi. Ad esempio, impedisce a Dick di stuprare delle ragazzine o aiuta un ragazzino in viaggio con il nonno. Da questo punto di vista Perry può essere definito un narcisista, poiché riesce a provare pietà soltanto per le persone sulle quali riesce a proiettare se stesso.

## Il mondo è malvagio

I malvagi del romanzo conoscono il mondo e il suo marciume, cui rispondono con cattiveria e violenza. Se il male è imprescindibile dal mondo, dunque la malvagità è la sua chiave di lettura. Dick e Perry si fanno così portatori del messaggio schopenhaeuriano _"la vita è solo dolore"_, ma aggiungono: _"chiunque tu uccidi, gli fai un piacere"_. Quello dei Clutter è stato un delitto senza motivo apparente, spinto da motivi irrazionali e assurdi. Vittima di un'eclissi mentale, quando Perry ha aggredito il signor Clutter stava di fatto distruggendo non un uomo, bensì _"una figura chiave di un'immagine traumatica del passato"_. Come ammise anche Perry stesso, egli non aveva intenzione di fargli del male, lo trovava persino simpatico... Fino al momento in cui non gli tagliò la gola: i Clutter non gli avevano fatto nulla di male, ma dovevano pagare per tutta la gente che lo _"aveva messo in croce per tutta la vita"_.

Significativa è la realizzazione che Dewey fa alla fine del romanzo: nonostante giustizia sia stata fatta, l'esecuzione degli assassini non lo fa sentire soddisfatto come avrebbe voluto. Come apprendiamo dal pensiero aristotelico, la catarsi delle passioni non può avvenire attraverso un'azione brutale. Capote è chiaramente contro la **pena di morte**. Ritenendo il male un qualcosa di imprescindibile dal mondo, pensa che mettere a morte i colpevoli non risolva nulla; tuttavia è un peccato che nell'opera non venga fornita una visione alternativa (ad esempio, per mezzo di interviste fatte a chi propone una procedura riabilitativa), in quanto l'autore si limita a criticare la pena di morte secondo un'ottica cristiana e moralista, che, come in tutto il romanzo, emerge dai _fatti_.

## Conclusioni

Come abbiamo visto, il romanzo di Capote è un'eccezionale macchina narrativa capace di assorbire svariati materiali (verbali di polizia, referti psichiatrici, sentenze, articoli di giornale ecc...) in un testo polimorfo che, nel finale, svela a chi legge l'enigma con cui si era scontrato nelle prime pagine, un enigma radicale sul senso dell'esistenza, del male, del dolore e della giustizia umana. L'opera è innovativa sia dal punto vista letterario sia ideologico non solo perché è una critica dell'_American Dream_, ma anche perché svela quella parte d'America considerata "scomoda", dunque il disagio, la povertà, le malattie mentali.

---

## Bibliografia

- Capote, Truman (2005). _A sangue freddo_, trad. it. di M. Ricci Dettore, Milano: Garzanti.
