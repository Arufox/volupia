---
title: "Intellettualizzazione dei film americani. Come il cinema entrò nel mondo dell'Arte negli USA"
date: 2022-09-02T14:29:24+02:00
description: "All’epoca della sua nascita il cinema americano veniva considerato puro intrattenimento e, in alcuni casi, anche volgare: nasce infatti come spettacolo popolare, in cui venivano spesso proiettati incontri di pugilato e gag comiche. Nei primi due decenni del XX secolo il cinema americano subì una grande trasformazione, diventando più sofisticato e sviluppando nuove forme convenzionali di produzione, riuscendo a conquistarsi un posto speciale nella cultura popolare americana."
featured: false
tags:
- cinema americano
- film
- arte
- consumismo
- hollywood
- cultura pop
toc: true
image: "/img/articoli/film.jpg"
imageCaption: Foto di <a href='https://unsplash.com/@tysonmoultrie' target='_blank'>Tyson Moultrie</a>
---

---

Oggi i film rappresentano una componente fondamentale del nostro intrattenimento. A quanti di noi non è mai capitato, dopo una giornata molto impegnativa, di voler accoccolarsi sul divano, magari in compagnia del proprio partner o di un cucciolo, e godersi la visione di un bel film?

Nonostante oggi il cinema venga unanimemente considerato la settima arte, ai suoi arbori la si pensava diversamente, soprattutto negli Stati Uniti: se in Europa il cinema ha iniziato precocemente a costruire un dialogo con le altre forme d'arte e quindi a essere riconosciuto come tale, negli Stati Uniti questo processo è stato più lento e faticoso.

In questo articolo verrà illustrato come avviene la legittimazione di una forma d'arte e quindi in che modo l’**intellettualizzazione dei film americani**, soprattutto negli anni Sessanta, favorì la consacrazione del cinema nel mondo dell’arte. Infine, verrà spiegato quanto le **modalità di consumo** dei prodotti culturali influiscano sullo _status_ sia di questi ultimi, sia dei loro consumatori.

## Dall'intrattenimento all'arte

All’epoca della sua nascita il cinema americano veniva considerato puro **intrattenimento** e, in alcuni casi, persino volgare: nasce come spettacolo popolare in cui venivano spesso proiettati incontri di pugilato e _gag_ comiche. I film venivano proiettati nei Concert Saloon, nei Nickelodeon e nei Vaudeville, pertanto all'epoca era idea diffusa che guardare film implicasse la frequentazione di luoghi poco raccomandabili. È nei primi due decenni del XX secolo che il cinema americano subì una grande trasformazione, diventando più sofisticato e sviluppando nuove forme convenzionali di produzione, riuscendo a conquistarsi un posto speciale nella cultura popolare americana.

Il regista David Griffith (1875-1948) ebbe il merito di aver innovato il cinema americano, gettando le basi di una “grammatica” per i film che comprendeva una serie di innovazioni tecniche e di tecniche drammatiche. La tecnica del montaggio, ad esempio, permise lo sviluppo di sequenze narrative che avvicinarono i film all’arte narrativa: il montaggio alternato in _Intolerance_ (1916) permise di unire tra loro spazi e tempi lontani e vicini per narrare la storia dell’umanità. È sempre grazie a Griffith, inoltre, che emersero per la prima volta le figure del regista e degli attori.

Prima della Grande Guerra negli Stati Uniti si proiettavano molti film europei, ma la situazione si ribaltò a causa del conflitto mondiale e della conseguente crisi economica dell’industria cinematografica europea: è così che Hollywood iniziò ad affermarsi a livello internazionale, la quale trasformò il cinema americano in un sistema industriale &mdash; lo _studio system_ &mdash; che produceva beni di consumo accattivanti. Il cinema cominciò allora a elevare il proprio livello culturale per raggiungere un pubblico sempre più ampio e a invocare per sé il riconoscimento quale forma d'arte.

L'avvento del televisore portò a Hollywood una crisi economica che cambiò profondamente il suo modo di fare cinema. Negli anni Quaranta era sempre più comune possedere un televisore e ciò fece sì che diminuisse l’affluenza in cinema e teatri. Inoltre, la diffusione del televisore anche negli strati inferiori della società, i quali preferivano guardare gli spettacoli d’intrattenimento dal divano di casa propria, rese l’andare al cinema un’attività più da persone colte. In questo modo quella che era sempre stata considerata una forma d’intrattenimento _middle-brow_ cominciava adesso ad aprirsi a un pubblico più variegato, abbracciando anche quello più istruito. 

Il motivo che portava i critici dell’epoca a escludere il cinema americano dal mondo dell’arte era il suo legame con il concetto di **entertainment**, la **cultura di massa** e il **consumismo**: la cultura standardizzata offerta dalle produzioni hollywoodiane teneva infatti lontani dalle sale gli strati sociali superiori (_high-brow_), i quali non si riconoscevano nei prodotti offerti dall’industria cinematografica. In tal senso, gli anni Sessanta rappresentarono una svolta poiché diedero ai film l’occasione di essere fruiti come opere d’arte, grazie soprattutto al movimento della **Pop Art** e alla nascita delle **Art House**.

L’**intellettualizzazione del cinema** portò difatti a una sua nuova concezione, a nuove forme di consumo e a nuove possibilità. Tuttavia, per convincere l’opinione pubblica sulla dignità artistica dei film, era importante che il cinema ricevesse il prestigio riservato alle opere d’arte; vennero così organizzati i festival del cinema, creati gli studi accademici dei _Film Studies_, esaltata la figura del regista come artista e fondati nuovi luoghi di fruizione per i film. In questo modo l’industria cinematografica poteva offrire nuove modalità di fruizione che permettevano al fruitore di distinguersi dalla massa, grazie a
un’esperienza di visione tutta intellettuale: i cinema Art House venivano promossi quali luoghi di svago in cui era possibile godersi l’arte e la cultura. Il **cinema d’arte**, con i suoi film
d’eccezione, diventò così una nuova alternativa al cinema _mainstream_.

## Come fa un'opera a diventare arte?

Prima di parlare di come il cinema americano sia stato legittimato a forma d'arte durante gli anni Sessanta, è opportuno cercare di dare una definizione, seppur incompleta, del **concetto di arte**. Nonostante il discorso attorno a tale concetto rimanga ancora aperto, molti esperti concordano sulla dimensione comunicativa ed emozionale dell’arte. Il filosofo Stephen Davies (1991), ad esempio, distingue due definizioni di arte: una funzionalista, ovvero che guarda alla sua funzione, e una procedurale, che guarda invece al processo attraverso il quale l’arte viene creata. Secondo la **definizione funzionalista**, l’opera d’arte adempirebbe una funzione, che di solito è quella di
offrire all’osservatore un’esperienza estetica appagante. La **definizione procedurale**, invece, ritiene che un’opera d’arte venga creata seguendo certe regole e certi procedimenti. Prendendo come esempio la Cappella Sistina, la reazione di ammirazione e riverenza che essa suscita nell’osservatore sta alla radice della definizione funzionalista, mentre un’opera come la _Fontana_ di Duchamp, che mette in discussione le convenzioni artistiche su cosa dovrebbe essere considerato arte, chiarisce perfettamente la visione procedurale di arte.

{{< figure
    src="/img/articoli/michelangelo.jpg"
    attr="Volta della Cappella Sistina (1508-1511), affrescata da Michelangelo Buonarroti, conservata ai Musei Vaticani, Roma. Foto di <a href='https://unsplash.com/it/@_calvincraig' target='_blank'>Calvin Craig</a>."
>}}

---

{{< figure
    src="/img/articoli/duchamp.jpg"
    attr="<em>Fontana</em> (1917), di Marcel Duchamp, conservata al Musée National d’Art Modern, Centre Pompidou, Parigi. Foto di <a href='https://www.flickr.com/photos/stankuns/' target='_blank'>Fernando Stankuns</a>."
    >}}

---

Però le opere d’arte vengono anche discusse da critici e storici, vengono esposte in musei e mostre come oggetti di apprezzamento estetico; pertanto ciò che in realtà conferisce a un’opera lo
**status di arte** è o il fatto che sia stata _creata_ da un **artista** o che sia stata _giudicata_ tale da chi ha **autorità** nel campo.

Tornando al discorso sul cinema, tracciare una definizione di arte è dunque utile per comprendere la posizione di intellettuali e studiosi nel discorso sulla legittimazione dei film di Hollywood quali forme d'arte. Ma cos’è che rende un film arte? Le caratteristiche che possono elevarlo a opera d’arte sono ad esempio la sua bellezza &mdash; sia in senso estetico sia in senso visivo &mdash;, l’innovazione legata alla sua creazione (editing, direzione
artistica, sceneggiatura, recitazione ecc…), i messaggi veicolati, l’espressione artistica dei registi. Un tale riconoscimento, inoltre, trasformerebbe un film in una speciale forma culturale cui è conferito un certo **prestigio** e tale status verrebbe attribuito anche ai suoi creatori e, da ultimi, i suoi fruitori. 

Tuttavia, quella che è stata spiegata finora non è che una visione filosofica che riconosce i film come opere d'arte dal punto di vista della loro qualità estetica e tecnica. Esiste però un altro approccio all’argomento, di tipo **sociologico**, che spiega quanto la produzione dei film e il loro **consumo**
all’interno di un **contesto sociale** siano coinvolti nell’elaborazione di uno spazio per il cinema all'interno del mondo dell’arte. Per comprendere in che modo lo status dei film hollywoodiani sia cambiato nel tempo, è infatti fondamentale capire quanto la cultura e la società americane abbiano inciso sul riconoscimento artistico dei film. Gli storici del cinema danno infatti grande importanza ai cambiamenti nella composizione degli spettatori e, nel caso del cinema americano, è soprattutto negli **anni Sessanta** che cominciò a formarsi il terreno fertile per la maturazione di un concetto di arte che comprendesse anche il cinema.

Quali furono i cambiamenti nella società americana che lo resero possibile? Il fatto che fin dalle sue origini il cinema americano era legato alla **classe operaia** (_low-brow_), ha giocato a sfavore di un suo riconoscimento artistico da parte degli intellettuali, ma i cambiamenti che attraversarono gli Stati Uniti nel Secondo Dopoguerra &mdash; la grande crescita demografica o la diffusione dell’educazione superiore e della televisione &mdash; portarono a una diversificazione del pubblico dei film, il quale si era fatto soprattutto più colto. La diffusione dell’istruzione superiore influì molto sullo status dei film, il cui riconoscimento è dipeso soprattutto da tre fattori: l’**associazione con un certo ceto sociale**, l’**opinione degli esperti** e la **capacità di ricezione** dei film. In molti casi l’associazione con un certo ceto sociale ha contribuito all’insuccesso o al prestigio di molte forme d'arte, la cui associazione ai ceti superiori ha sempre giovato al loro status; di conseguenza, l'avvicinamento della classe _high-brow_ ai film ne favorì la legittimazione artistica. 

Gli studiosi ritengono che la **gerarchia culturale** venga a costruirsi a partire dal **modo** in cui l’arte viene consumata e dallo **status sociale** dei suoi consumatori. Il ceto superiore, inoltre, teme il rischio di una contaminazione simbolica causata dal consumo di prodotti culturali associati al ceto popolare: in ambito musicale, ad esempio, gli esponenti dell'alta società preferiranno ascoltare la musica classica o di musicisti sconosciuti, piuttosto che i brani pop ascoltati dalla massa.

{{< figure
    src="http://boingboing.net/wp-content/uploads/2014/06/png.png"
    alt="Le differenze nei gusti dal _low-brow_ al _high-brow_"
    attr="Classificazione del grado di raffinatezza dei prodotti culturali in base alla classe sociale che li consuma. Fonte: LIFE, uscita del 11/04/1949, pp. 100-101."
>}}

---

Un’eccezione è tuttavia possibile solamente quando il divario tra ceto alto e basso è abbastanza ampio da non permettere una confusione tra i due gruppi. Sempre parlando di musica, tale pericolo infatti non esiste se ad essere consumata è la musica di una subcultura, proprio in virtù del fatto che non potrà mai essere associata alla cultura _high-brow_. Ad esempio, se negli anni Trenta si accese tra i ricchi un grande entusiasmo per la musica jazz, è proprio per via della distanza tra bianchi e neri: era infatti impossibile che il consumo del jazz potesse accostare il bianco borghese alla cultura afroamericana.

Tale concetto può essere applicato anche al consumo dei film, che la classe _high-brow_ ha iniziato ad apprezzare una volta che la massa smise di andare al cinema. Il principale motivo per cui nella prima metà del XX secolo gli intellettuali snobbavano i film hollywoodiani era che questi film erano troppo vicini alle forme culturali popolari, di massa o folkloristiche. Inoltre, la tecnologia impiegata nella creazione dei film era persino d’ostacolo a ogni tentativo d’intellettualizzazione del _medium_: impiegati dalle nuove industrie culturali per generare **profitto**, i nuovi _media_ venivano esclusi dai discorsi sull'arte.

È solamente alla fine degli anni Cinquanta che, grazie al movimento della **Pop Art**, le cose iniziarono a cambiare: la cultura di massa fu rivalutata e sorsero nuove prospettive sul ruolo e il valore dell’arte. A essere criticata adesso era proprio l’arte tradizionale, dato che la ricerca artistica si concentrava sull’**estetica della vita quotidiana** e sulle **persone comuni**. Così la Pop Art vedeva arte persino negli oggetti quotidiani e nelle macchine, cercando di sovvertire le distinzioni tra cultura alta e cultura bassa.

La distinzione tra cultura alta e cultura bassa si basa essenzialmentie sulle differenti **modalità di consumo** e sul **discorso critico** sviluppato attorno all'arte. Ad esempio, la **cultura popolare** viene consumata in modo diretto, senza ricorrere a esperti, mentre l’**arte** necessita di una mediazione di questi ultimi. La **valutazione degli esperti** è dunque il fattore cruciale che distingue la cultura popolare dall'_alta arte_: il termine stesso suggerisce che si tratta di un tipo di arte elitaria, di non facile accesso all'individuo ineducato alla sua fruizione.

{{< mquote content="To participate in high art is to forgo the direct and unmediated perception of the artwork itself. The principal consequence is the dependence of one’s own judgement of artistic quality on the judgement of others. […] [It is to give up] partial rights of control of one’s own judgement to experts in exchange for the higher status that competent talk about these artworks provides. The status bargain, then, is _an exchange of prestige for opinion rights_."
translation="Partecipare all'_arte alta_ significa rinunciare alla percezione diretta e immediata dell'opera stessa. La principale conseguenza è che il nostro giudizio sulla qualità artistica di un'opera dipenderebbe dal giudizio degli altri. [...] Significa cedere parzialmente agli esperti il diritto di controllare il nostro giudizio personale in cambio dello status che il parlar competente sulle opere d'arte conferisce. La negoziazione dello status è dunque _uno scambio tra prestigio e diritto di opinione_."
translationAuthor="Shrum 1996: 9. Traduzione di Alessandra Gallia"
>}}

L’argomentazione di Welsey Shrum (1996) è utile per comprendere come l’opinione dei critici contribuisca alla legittimazione delle opere d'arte. Così &mdash; sempre negli anni Sessanta &mdash; i film vennero rivalutati grazie a una maggiore stima nei confronti dei **critici cinematografici**, le cui opinioni cominciarono a essere considerate importanti anche dal pubblico. Le recensioni dei film guidano infatti il pubblico alla visione dei film, fornendo gli strumenti utili a interrogarli. Tuttavia è anche vero che l'opinione degli esperti può essere strumentalizzata per fini commerciali, in modo da indirizzare il consumatore verso alcuni prodotti. Oggi è infatti pratica comune citare alcune recensioni nella pubblicità dei film.

### Il ruolo delle istituzioni

La creazione di un **campo culturale** per una data espressione culturale eleva quest'ultima agli occhi delle persone, poiché ne motiva le intenzioni. Lo scopo di un campo è difatti convincere il resto del mondo che ciò che viene fatto è arte e dunque merita gli stessi diritti e privilegi associati a tale status. Un campo è un universo sociale con leggi proprie e funziona indipendentemente dalla politica e dall’economia. Naturalmente gli artisti che vi appartengono conoscono il complesso sistema di valutazione che regola un campo e, nel caso del **campo cinematografico**, esso comprende i **festival**, la **critica**, le **recensioni**, la **censura** e i **modi di produzione**.

Le **istituzioni** giocano un ruolo fondamentale nella legittimazione di una forma d'arte, poiché è a partire da loro che comincia a costruirsi il discorso critico. Il **discorso accademico** sui film fu sviluppato nelle università americane tra il 1960 e il 1980, eppure le università di cinema esistevano già da prima e avevano lo scopo di formare i nuovi registi. Fu allora lo studio della storia e della teoria del cinema a portare alla creazione di un **canone** per i film e registi come Buster Keaton, Alfred Hitchcock, David Griffith, Ernst Lubitsch, Orson Welles, Federico Fellini, Ingmar Bergman, Friedrich Murnau, Michelangelo Antonioni e molti altri cominciarono a essere studiati nei libri di testo.

Altra componente importante del mondo dell’arte sono, infine, i **premi**, in quanto vengono assegnati da esperti del campo e sono pertanto indice di **merito artistico**. In ambito cinematografico i **festival** sono eventi in cui viene dimostrato il valore artistico dei film: vincere la Palma d’Oro a Cannes, il Premio della Giuria al Sundace, o avere anche solo la _nomination_ a uno di questi premi, rappresenta un tratto distintivo di qualità che tutti riconoscono.

{{< figure
    src="/img/articoli/premio.jpg"
    alt="Diversi premi cinematografici"
    attr="Immagine di <a href='https://it.freepik.com/vettori-gratuito/il-film-realistico-premia-gli-ornamenti_23820921.htm#query=oscar&position=19&from_view=search&track=sph' target='_blank'>pikisuperstar</a>"
>}}

---

La percezione su che cosa possa essere considerato arte è dunque influenzata da un’ideologia che legittima un prodotto culturale agli occhi del mondo. Difatti fu soltanto quando venne a costruirsi un discorso teorico sull'apprezzamento estetico dei film, che il cinema americano entrò finalmente nel mondo dell'arte negli Stati Uniti; inoltre tali teorie sono in grado di influenzare la percezione e l'opinione del pubblico.

## Il cinema tra arte e mestiere

All’inizio del Novecento, quando i film non erano che giustapposizioni di scene, il discorso sui film era di tipo descrittivo ed esprimeva curiosità e divertimento verso questa
nuova forma d’intrattenimento. L’adozione del formato narrativo rese possibile parlare anche delle storie che venivano messe in scena, portando i recensori a concentrarsi sulla qualità della realizzazione tecnico-narrativa e della recitazione.

Già negli anni Trenta alcuni movimenti d’avanguardia scrivevano che alcuni film avevano qualcosa di artistico e geniale, come ad esempio _Luci della città_ (1931) di Charlie Chaplin. Nonostante ci fosse un gran numero di intellettuali americani che vedeva nel cinema un grande potenziale artistico, i film di Hollywood non stavano molto in alto nella gerarchia delle arti. Il riconoscimento artistico dei film veniva difatti accordato solo alle produzioni europee; inoltre era stata la proiezione nelle sale americane del corto _Il Miracolo_ (1948) di Roberto Rossellini a portare la Corte Suprema a dichiarare nel 1952 che _"it cannot be doubted that motion pictures are a significant medium for the communication of ideas"_.[^1] Questo evento viene ricordato nella storia come _Miracle decision_ e negli Stati Uniti portò a un allentamento della censura dei film, dando una maggiore libertà ai registi di trattare anche tematiche controverse.

Tuttavia i film non godono ancor oggi dello stesso prestigio riservato alla letteratura o alla pittura. Il motivo potrebbe essere la mancanza di una chiara distinzione tra i generi come esiste per la musica e il teatro, i cui generi hanno caratteristiche ben precise e distinte (basti pensare, ad esempio, alle differenze tra l’opera e il musical, o tra la musica classica e il rock). Per i film si fa fatica a distinguere ciò che potrebbe essere considerato arte da ciò che invece è puro intrattenimento. Generalmente i film vengono divisi in “artistici” e “commerciali”, classificandoli a seconda che essi siano “hollywoodiani” o “indipendenti”. Tuttavia “Hollywood” e “arte” non devono essere considerate due categorie che si escludono a vicenda: film come _Quarto potere_ (1949), _Psycho_ (1960), _2001: Odissea nello spazio_ (1968), _Il Padrino_ (1972) e molti altri classici sono considerati arte, eppure sono film hollywoodiani.

Ciò che danneggia l’immagine dei film hollywoodiani come arte è il **business** che vi sta dietro, perché a differenza dell’arte (che in teoria non dovrebbe avere scopi di lucro), la produzione e la promozione dei film sono gestite dagli _Studios_ che li hanno finanziati e che ovviamente vogliono trarne profitto, e nemmeno le produzioni indipendenti e i registi in questo senso disinteressati sono esclusi da tale meccanismo. Già a partire dagli anni Quaranta Hollywood produceva film anche per soddisfare i suoi mercati obiettivo ed era molto comune che gli _Studios_ promuovessero i loro film grazie ad altre industrie, usando ad esempio gli attori più popolari come sponsor di prodotti, come sigarette o dentifrici.

{{< figure
    src="/img/articoli/cigarette.jpg"
    attr="Foto di <a href='https://unsplash.com/it/@kosh_d1' target='_blank'>Dima Kosh</a>"
    >}}

---

È allora opportuno introdurre due concetti che fanno parte del discorso sui film: i concetti di arte e mestiere. Un **mestiere** viene valutato a seconda che il suo lavoro soddisfi una **funzione** e che l’artigiano abbia abilità magistrali e crei **bellezza**; l’**arte** invece viene valutata in base alla creatività, all’espressività e all’**unicità** di un’opera. È vero che la bellezza di un film è data dall’abilità tecnica con cui è stato realizzato, ma è l’**espressione personale del regista** e il **messaggio** che vuole comunicare ciò che rende un film arte.

## Il cinema d'arte negli Stati Uniti

Fino agli anni Cinquanta la produzione dei film era controllata dallo _studio system_, il quale esercitava un grande controllo sul lavoro creativo dei registi. Con la fine del monopolio esercitato dalle _Major_ sui teatri (ovvero i _movie theater_), decretata dalla Corte Suprema nel 1948 con la _Paramount decision_, tale sistema di produzione cominciò a essere abbandonato, lasciando maggiore spazio ai registi indipendenti. Grazie a questo cambiamento, la figura del **regista** cominciò a emergere come artista, il che portò a un nuovo modo di concepire i film ispirato alla Francia. 

Negli anni Cinquanta il movimento francese della Nouvelle Vague elaborò un nuovo approccio ai film che, successivamente, ispirò la critica cinematografica americana. Tale approccio veniva chiamato _politique des auteurs_ (che negli Stati Uniti prese il nome di _auteurism_), il quale vedeva nella figura del regista la principale forza creativa dei film, capace di creare secondo la
propria visione personale pur rispettando i limiti economici e organizzativi della produzione. Grazie all’**auteurism** i film cominciarono a essere apprezzati più come creazioni di artisti che
come prodotti di un’industria. Ciononostante, rimane pur sempre una domanda da porsi: _in che modo l’industria cinematografica ha sfruttato questa nuova percezione dei film?_

Il benessere economico raggiunto dopo la Grande Depressione rese più labili i confini tra le classi sociali americane, il che portò i consumatori di _cultura alta_ a sentire un maggior bisogno di distinguersi dalla massa, richiedendo prodotti più raffinati il cui consumo indicasse la _superiorità_ della loro classe sociale. L’industria cinematografica cercò di sfruttare simili richieste offrendo ai consumatori più raffinati un modo per distinguersi dal pubblico della classe media: nasce così il **cinema d’arte**, il quale offriva un’esperienza di visione tutta intellettuale, promuovendo un’immagine di _leisure_ intellettuale e artistico.

Persino i luoghi in cui i film venivano proiettati giocavano un
ruolo importante nel riconoscimento dei film come arte, pertanto film come _Roma città aperta_ (1945) avevano bisogno di essere proiettati in un luogo diverso rispetto ai film _mainstream_. Ad esempio, il movimento dei **cinema Art House** &mdash; iniziato già negli anni Cinquanta e diffuso negli anni Sessanta &mdash; si rivolgeva a un pubblico di amanti del cinema, i quali consideravano i film una forma artistica al pari della letteratura o della danza. Questi cinema proiettavano film indipendenti, sperimentali, controversi o documentari e si trattava di piccoli teatri che avevano una galleria d’arte nell'atrio e offrivano ai loro clienti caffè e film brillanti a un prezzo esclusivo: chi frequentava le _Art House_ voleva assolutamente distinguersi dal pubblico ordinario.

{{< figure
    src="/img/articoli/cinema.jpg"
    attr="Foto di <a href='https://unsplash.com/it/@nypl' target='_blank'>The New York Public Library</a>"
    >}}

---

Per comprendere il ruolo del cinema d’arte nella società americana bisogna inquadrare i film d’arte come genere, interrogandoci su ciò che rendeva un film appartenente a
questa categoria. Molti studiosi hanno dato definizioni incomplete o ambigue ai film d'arte: ad esempio, Peter Lev (1993) stabilisce che questi film devono esprimere idee e contenuti nuovi, tuttavia tale definizione risulta poco chiara, in quanto può significare cose diverse in base al periodo storico preso in esame. C'è però una cosa su cui tutti gli studiosi concordano, ovvero che i film d’arte _non_ sono film _mainstream_. Come scrive David Bordwell (1979):

{{< mquote content="The art cinema defines itself explicity against the classical narrative mode and especially against the cause-effect linkage of events."
translation="Il cinema d'arte viene a delinearsi esplicitamente in contrasto alla modalità narrativa classica e soprattutto alla correlazione causa-effetto degli eventi."
translationAuthor="Traduzione di Alessandra Gallia"
>}}

La particolarità dei film d’arte è dunque nella **forma** e nel **linguaggio**. Se i film _mainstream_ sono di facile lettura, con una trama lineare e personaggi motivati a raggiungere obiettivi chiari e specifici, i film d’arte hanno invece una struttura più complessa e ambigua, l’impronta del regista è ben evidente e intrattengono un rapporto dialogico con lo spettatore. Altra caratteristica attribuita ai film d’arte è quella del **realismo**, ovvero il realismo delle tematiche affrontate (ad esempio i problemi del mondo reale, spesso di carattere sociale), del set (scene riprese dal vero), della recitazione (improvvisazione, uso del dialetto) e di una **narrazione senza veli** (ovvero scene di sesso esplicito o di violenza). Tali caratteristiche formali rendevano questi film adatti a un pubblico adulto e più maturo, che ricercava nei film un’esperienza intellettuale, estetica e catartica.

D'altro canto il cinema d’arte nasce come reazione a una Hollywood che dominava il mercato internazionale, soprattutto in quei paesi dove l’industria nazionale era stata
danneggiata dalla Prima Guerra Mondiale, come nel caso di Francia, Germania, Italia e Inghilterra. Dato che la produzione, la distribuzione e la proiezione dei film erano sempre legate a
un contesto di confini nazionali, culturali ed economici predefiniti, il dominio internazionale di Hollywood veniva concepito dagli stati europei come un “problema nazionale”. Nei vari paesi furono allora articolate politiche volte alla ricostruzione prima di un’**industria cinematografica nazionale** con le proprie tradizioni, poi di una **cultura nazionale**. I film, difatti, partecipano attivamente alla costruzione dell'identità nazionale e i film d’arte servirono proprio a definire quelli che divennero il cinema italiano, il cinema francese, quello tedesco e così via.

Ciononostante, il cinema d’arte riuscì comunque ad assicurarsi una nicchia all’interno del mercato internazionale e, grazie alle sue aspirazioni estetiche e culturali, gli fu riconosciuto persino lo status di arte. Questi film condividono la visione romantica dell’arte come espressione soggettiva dell’artista, in sintonia con le concezioni delle istituzioni culturali; inoltre tendevano sempre a conservare i tratti distintivi del loro paese d'origine e all’estero venivano spesso sottotitolati, piuttosto che doppiati. Negli Stati Uniti la concezione più comune di cinema d’arte era legata al concetto d'indipendenza di tali produzioni; inoltre era il paese d'origine di un film a determinarne lo status: i **film europei** (soprattutto quelli francesi e italiani) erano riconosciuti come arte, mentre i film non europei venivano invece considerati “film etnici”.

### L'industria dell'arte

Nonostante l’ispirazione artistica che lo motiva, il cinema d’arte fa comunque parte di un’**industria capitalista** e deve dunque negoziare continuamente con il **mercato**, pur mantenendo la propria immagine. L’_autorism_ riconosciuto in questo genere di film divenne presto un **marchio di fabbrica**: il nome del regista fungeva difatti da _brand_ per i film diretti dallo stesso, capace di orientare le aspettative degli spettatori. Persino Hollywood cercò di sfruttare a proprio vantaggio questo mercato di nicchia e film come _Ladri di biciclette_ (1948), _Scarpette rosse_ (1948), _Il posto delle fragole_ (1957), _Piace a troppi_ (1956), _8½_ (1963) e molti altri film d’arte europei vennero usati come modello per produrre i nuovi film hollywoodiani: vennero così prodotti _L'uomo dal braccio d'oro_ (1955), _Orizzonti di gloria_ (1957) e _La parete di fango_ (1958) per un pubblico più colto. Le _Major_, inoltre, associarono il proprio nome ai film d’arte, producendo film in Europa &mdash; come _Eyes Wide Shut_ (1997) &mdash; o distribuendo negli Stati Uniti quelli stranieri, come _La vita è bella_ (1997, Miramax) o _The Full Monty_ (1997, 20th Century-Fox). Tale accostamento al cinema d’arte portò chiaramente nuovo prestigio a Hollywood, il che poteva essere
sfruttato grazie a strategie di marketing.

Come possiamo vedere, nasce anche qui un contrasto tra arte e business: nonostante gli interessi commerciali dietro a tale industria, i film d’arte sono comunque mossi principalmente da
ispirazione artistica, però è anche vero che i loro registi, distributori ed esibitori guadagnano grossi benefici dall’attrazione del pubblico per i film “non commerciali” e “artistici”, non solo in termini di capitale economico, ma soprattutto di **capitale simbolico**. Nonostante il cinema d’arte
fosse una nicchia all’interno di un’industria cinematografica dominata da Hollywood, i benefici economici che il capitale simbolico poteva fruttargli nel tempo potevano essere ottenuti solamente se questa nicchia fosse rimasta tale: il successo e il prestigio dei film d’arte si doveva infatti alla loro _separazione_ dal cinema commerciale e d’intrattenimento, oltre che alla loro motivazione artistica.

## Conclusioni

In questo articolo si è parlato di quanto l’intellettualizzazione del cinema abbia contribuito al riconoscimento artistico dei film negli Stati Uniti, soprattutto a partire dagli anni Sessanta. Se all’inizio della sua storia il cinema americano era considerato puro intrattenimento, dunque incapace di trattare tematiche serie, a partire dagli anni Venti esso cercò d'innalzarsi culturalmente e di smorzare tale pregiudizio. Per legittimarsi artisticamente, il cinema aveva bisogno innanzitutto di accostarsi alla cultura alta, di adeguarsi alle forme della cultura modernista e di rivendicare la propria unicità artistica. Da qui emerse, negli anni Sessanta, una nuova visione che concepiva i film come espressione artistica del regista, cambiando anche il modo di consumare i film: nacquero allora appositi teatri &mdash; le _Art House_ &mdash; che proiettavano film che immergevano lo spettatore in un contesto artistico e intellettuale; vennero fondati i _Film Studies_ e i festival del cinema, grazie ai quali i film diventarono espressione della **cultura alta**.

La riluttanza che fino ad allora aveva impedito al cinema hollywoodiano un riconoscimento artistico era dovuta al fatto che esso era legato alla cultura di massa e alla classe medio-bassa. Oltre al lavoro di intellettuali e istituzioni nella rivalutazione dei film, era stato necessario cambiare anche le **modalità di consumo** di questa forma culturale, in quanto l’approccio del consumatore nei confronti del prodotto è considerato determinante nel conferimento di uno status: il cinema americano doveva infatti allontanarsi dalla massa ed
essere consumato dalla classe _high-brow_ per essere definito arte.

Discutere sui generi dei film permette di ricavare informazioni sul loro contesto di produzione e ricezione: etichettare, ad esempio, un film come “classico di Hollywood” oppure come “film d’arte” fornisce informazioni sulle proprietà del testo filmico, sulla sua produzione, distribuzione e proiezione. Per comprendere il ruolo del cinema d’arte nella società americana si è cercato d'inquadrare il **film d’arte** come genere, riflettendo brevemente su ciò che rende un film appartenente a questa categoria. Oltre alle caratteristiche tecniche e formali che lo contraddistinguono, alla base del cinema d’arte c’è essenzialmente un **meccanismo di discriminazione**: la discriminazione nei confronti dell’industria, dell’intrattenimento e del profitto in favore dell’arte, della cultura e del senso. Tale caratteristica permette di riconoscere a questi film un certo livello di arte, cultura e qualità, garantendogli inoltre un’**indipendenza** economica, ideologica ed estetica all’interno dell’industria cinematografica stessa.

Nonostante gli studiosi abbiano dato molte definizioni al concetto di arte e fatto molte analisi che cercassero di studiare il funzionamento di tale mondo, la verità è che questo discorso rimane &mdash; e deve rimanere &mdash; aperto. Personalmente non credo che sia possibile dare una risposta definitiva alla domanda
_“che cos’è l’arte?”_ proprio perché tale concezione cambia nel tempo. Ad esempio, tempo fa nessuno avrebbe mai immaginato che un orinatoio potesse essere esposto in un museo d’arte, eppure…

---

## Bibliografia

- Baumann, Shyron (2007). _Hollywood Highbrow. From Entertainment to Art_, Princenton, Oxford: Princenton University Press.
- Becker, Howard (1982). _Art Worlds_, Berkeley: University of California Press.
- Bordwell, David (1979). “The Art Cinema as a Mode of Film Practice”, _Film Criticism 4_: 56-64.
- Bordwell, David (2017). _Reinventing Hollywood. How 1940s Filmmakers Changed Movie Storytelling_, Chicago and London: University of Chicago Press.
- Davies, Stephen (1991). _Definitions of Art_, Ithaca, New York: Cornell University Press.
- Lev, Peter (1993). _The Euro-American Cinema_, Austin: University of Texas Press.
- Lopes, Paul (2002). _The Rise of a Jazz Art World_, Cambridge: Cambridge University Press.
- Neale, Steve (1981). “Art Cinema as Institution”, _Screen 22_: 11-39.
- Randall, Richard S. (1968). _Censorship of the Movies: The Social and Political Control of a Mass Medium_, Madison: University of Wisconsin Press.
- Shrum, Welsey (1996). _Fringe and Fortune: The Role of Critics in High and Popular Art_, Princeton: Princeton University Press.
- Wilinsky, Barbara (2001). _Sure Seaters. The Emergence of Art House Cinema_, Minneapolis and London: University of Minnesota Press.

---

## Note

[^1]: V. Randall 1968.