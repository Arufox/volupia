---
title: "Georg C. Lichtenberg e l'arte dell'aforisma"
date: 2022-11-30T18:36:51+02:00
description: "Professore di fisica alla Georg-August-Universität di Gottinga, Lichtenberg era uno degli intellettuali più importanti e popolari dell'Europa illuminista, nonché grandissimo autore di aforismi. Anche se probabilmente non avete mai sentito parlare di lui, lo avrete incontrato in modo indiretto, attraverso le opere dei grandi pensatori europei moderni e contemporanei."
featured: false
tags:
- aforismi
- letteratura tedesca
- teoria dei sogni
- illuminismo
- gottinga
- zibaldone
- diari
toc: true
image: "/img/articoli/diario.jpg"
imageCaption: "Foto di <a href='https://unsplash.com/@kiwihug' target='_blank'>Kiwihug</a>"
---

---

Georg C. Lichtenberg (1742-1799) non è così poco conosciuto come si potrebbe pensare. Professore di fisica alla Georg-August-Universität di Gottinga, famoso per le ricerche sull'elettricità (le figure di Lichtenberg), era uno degli intellettuali più importanti e popolari dell'Europa illuminista.

Il più grande esperto dei manoscritti lichtenberghiani Ulrich Joost ha raccolto ben 900 testimonianze di letture a opera dei contemporanei dell’illuminista tedesco, come Kant e Goethe. Così, tramite gli scritti di questi ultimi, molti loro lettori sono entrati in contatto con il lavoro di Lichtenberg. Grande autore di **aforismi**, ci si può infatti imbattere in lui nelle letture più diverse, basti pensare ad esempio alle opere di Breton, Freud, Tolstoj o Einstein.

Indipendentemente dalla conoscenza del tedesco, qualunque persona di cultura può dunque imbattersi, anche se "filtrato", in Lichtenberg, leggendo le opere dei grandi pensatori europei, moderni e contemporanei. Come scrisse Karl Kraus (1972), Lichtenberg _"scava in profondità più di ogni altro [...]. Parla sottoterra. Solo chi scava ugualmente in profondità riesce a udirlo. Non va per niente a scapito del rispetto di Schopenauer, se talvolta si avvertono le verità dei suoi scritti brevi come fossero dei rumori"_.

## L'aforisma come genere letterario

Il genere dell'aforisma ha una storia lunghissima, che ha le proprie radici nell'antica Grecia. Gli _Aforismi_ (V-IV sec. a.C.) di Ippocrate sono considerati la più antica raccolta di aforismi, contenente molti precetti medici e pensieri del medico greco. Nei suoi testi, difatti, l'**aforisma** viene inteso come una forma letteraria rivolta alla cura del corpo, sicché nel Medioevo con tale termine si indicava lo studio e la pratica della **medicina**.[^1]

Per parlare degli _Zibaldoni_ di Lichtenberg e delle sue brillanti osservazioni, dobbiamo prima collocare il genere dell'aforisma nel contesto culturale della Germania illuminista del XVIII secolo. Sebbene all'epoca non esistesse ancora un’organizzazione precisa dei testi in generi letterari (ted. _Gattungen_), i parametri della critica letteraria del tempo erano di **matrice idealista**: in linea, dunque, con la concezione hegeliana di "libero capolavoro poetico", i "funzionari della letteratura" &mdash; come venivano chiamati ironicamente da Musil &mdash; erano ostili a ogni forma di frammentarietà e di logica non discorsiva. Difatti, l'**aforisma** apparteneva a un genere definito **vermischte Schriften** (letteralmente “scritti misti, miscellanee”), con il quale si indicava una scrittura priva di norme codificate, con grande libertà stilistica e di sperimentazione formale e concettuale. Venivano allora chiamati _vermischte Schriften_ la scrittura aforistica, saggistica ed epistolare, i diari contenenti bozze, idee, riflessioni. Così, testi come le _Fantasie patriottiche_ (1786) di Justus Möser, _Sulle relazioni umane_ (1788) di Adolph Knigge, le _Lettere per la promozione dell'umanità_ (1797) di Johann G. Herder o gli _Zibaldoni_ di Lichtenberg, sono da considerarsi esempi di _vermischte Schriften_ del XVIII secolo.

Saggista, diarista, epigrammista ed epistolografo esemplare, affascinante per la commistione di generi letterari, Lichtenberg veniva spesso accusato dai suoi contemporanei di non riuscire a raggiungere un tutto organico nei propri scritti. Le sue _vermischte Schriften_ comprendono difatti lettere, aforismi, annotazioni scientifiche e una ricca serie di contributi per riviste come le _Göttingische Gelehrnte Anzeigen_ (Bollettino d'erudizione di Gottinga), l'_Hamburgisches Magazine_ (Rivista di Amburgo) o il _Göttinger Taschen-Calender_ (Calendario Tascabile di Gottinga).

L’aspetto affascinante della scrittura aforistica di Lichtenberg è che si tratta di scritti che contengono sempre osservazioni importanti, scoperte utili che l'autore non ha il tempo o la voglia di approfondire. Non potendo servire alla stesura di un libro, gli aforismi (dal gr. _aphorízo_ "definire, limitare, confinare") riflettono fedelmente il concetto di _brevitas_: sono osservazioni lapidarie che nella loro concisione e acutezza colpiscono il lettore come un fulmine a ciel sereno. Werner Helmich (2006) definisce l'aforisma una _"forma letteraria di prosa, concisa, isolata da un contesto, priva di finzione narrativa e provvista di una '_ pointe _', cioè di un effetto stilistico destinato a procurare nel lettore una sorpresa estetica o gnoseologica"_.

Oggi il termine aforisma viene spesso usato impropriamente per indicare qualunque tipo di frase presa isolatamente da un testo &mdash; che in realtà è una **citazione** &mdash; quando invece andrebbe riservato per pensieri concisi e illuminanti, creati in quanto tali dai loro autori. L'aforisma è difatti una massima, sentenza o definizione che in parole brevi e sentenziose riassume il risultato di considerazioni, osservazioni ed esperienze. Lichtenberg conosceva bene la differenza tra la lingua parlata e quella scritta e sapeva quanto fosse difficile _"scrivere bene"_, così come _"scrivere in modo naturale"_ fosse un'arte: in E 38 sostiene infatti che se solo si dedicasse del tempo a soppesare tutte le parole, si potrebbero ottenere gli esiti che resero Tacito immortale.

## Gli zibaldoni di Lichtenberg

Lichtenberg raccolse aforismi e annotazioni di natura scientifica in taccuini (ted. _Hefte_), scritti dal 1764 al 1799. È al fratello maggiore Ludwig C. Lichtenberg, il quale ricevette i taccuini alla morte dell’autore, che si deve la prima pubblicazione degli zibalboni lichtenberghiani.

La prima edizione dei taccuini, dal titolo _Vermischte Schriften_, fu pubblicata nel 1800-1801 con l'aiuto di Friedrich Kries, e si componeva di 'soli' 9 volumi. Successivamente, in occasione del centesimo compleanno di Lichtenberg, nel 1844 i suoi figli pubblicarono una nuova edizione dei taccuini, estesa a 14 volumi,
che tuttavia escludeva le annotazioni scientifiche dell'autore. 

Queste edizioni del XIX secolo dei taccuini di Lichtenberg contengono i suoi aforismi, i quali sono stati organizzati per tematiche senza però tenere in considerazione l’ordine cronologico con cui furono scritti. Nonostante l’edizione del 1844 comprendesse frammenti inediti rispetto alla precedente, si è dovuto constatare che i manoscritti erano incompleti: purtroppo i Taccuini G, H e la maggior parte di K sono andati perduti dopo la morte dell’autore, e secondo alcune ipotesi, i taccuini in questione sarebbero stati distrutti dalla famiglia stessa, per evitare che fatti scomodi potessero venire alla luce.

Tra il 1902 e il 1908 fu fatta, a opera del germanista Albert Leitzmann, una nuova selezione dei taccuini di Lichtenberg. Fu così pubblicata la loro prima edizione critica, con il titolo _Georg Christoph Lichtenbergs Aphorismen_. Per questa nuova edizione Leitzmann abolì il principio che organizzava gli aforismi per tematiche e li riordinò secondo l’ordine cronologico originale, riportato sui taccuini. Il germanista riuscì inoltre a convincere la famiglia Lichtenberg a donare i manoscritti alla Biblioteca dell’Università di Gottinga per la ricerca. Tuttavia, anche questa nuova edizione escludeva le annotazioni scientifiche &mdash; che corrispondono a circa il 25% delle annotazioni &mdash; perché queste ultime, secondo Leitzmann, non sarebbero che _"appunti privi di ogni interesse"_.[^2] Quest'ultima affermazione del germanista rispecchia perfettamente l'atteggiamento ostile della critica letteraria tedesca nei confronti delle _Zweckformen_, cioè quei testi che intendono sondare problemi di varia natura (saggi, critica, diari, trattati ecc.). Gli studiosi di letteratura hanno infatti dibattuto per anni sul problema della distinzione dei testi "letterari" da quelli "non letterari".

È solamente grazie a Wolfgang Promies se oggi conosciamo la fisionomia originale dei taccuini di Lichtenberg. A partire dal 1968 il germanista ne pubblicò una nuova edizione dal titolo
_Sudelbücher_ (Zibaldoni), che contiene integralmente tutto quel che si è conservato dei manoscritti: dal Taccuino A (cinque quaderni in ottavo)[^3] al Taccuino L. Inoltre, l’abbandono del termine "Aphorismen" in favore di "Sudelbücher" esprimerebbe un cambio di prospettiva della filologia: se al genere dell'aforisma appartenevano brevi massime enuncianti regole pratiche o norme di saggezza, la parola _Sudelbuch_ (zibaldone) era stata scritta da Lichtenberg stesso sulla copertina del Taccuino F. La nuova prospettiva che uno zibaldone apre davanti ai nostri occhi, ci fa comprendere quanto fosse grande l'erudizione di questo scienziato di Gottinga. Dal punto di vista di Promies (1992), abbandonare il termine _Aphorismen_ permette di accogliere anche _"appunti di natura squisitamente privata,_ Excerpta[^4] _di altri autori, calcoli matematici, osservazioni e riflessioni su argomenti pertinenti a quasi tutti gli ambiti delle scienze, [...] liste di libri da leggere, [...] disegni e scarabocchi"_.

## Sull'importanza della lettura

Nel suo tributo a Lichtenberg, Elias Canetti (1973) definisce l'intellettuale di Gottinga _"una pulce con lo spirito di un uomo"_, proprio perché le letture non placano la sua _fame_ di curiosità, bensì gli permettono di “saltare” in tutte le direzioni, nei diversi ambiti del sapere. L'illuminista tedesco paragonava infatti la lettura al processo della digestione:

>Non c'è niente che spieghi l'attività della lettura e dello studio meglio del mangiare e del digerire. Il vero lettore-filosofo non si limita ad ammassare nella memoria come il divoratore che si riempie la pancia; al contrario chi esercita solo la memoria finisce per avere la pancia piena, ma non un organismo forte e sano. Nel primo caso, tutto ciò che si legge e si trova fruibile è distrutto, per così dire, un po' qua, un po' là, all'interno dell'organismo intero, sicché è il tutto ad acquistarne vigore. (F 203)

Usando la stessa metafora, Canetti ritiene che Lichtenberg "digerisca" bene tutto quel che legga o che studi, senza risultarne "appesantito" o "ingrassato", parlando di una _"erudizione leggera come la luce"_. Lichtenberg distingueva inoltre chi leggeva attivamente da chi leggeva passivamente: leggere attivamente significa inventare da ciò che si è letto, mentre la lettura passiva non è che barbara bramosia di riempire la propria mente (_"A forza di leggere siamo diventati dei barbari istruiti"_ [F 1085]). In questo modo Lichtenberg criticava anche la _Lesenwuth_, ovvero la bramosia di riempire la mente di letture anziché di rafforzarla.

Per rafforzare il proprio cervello con la lettura è necessario avere una buona “digestione spirituale”, ovvero la capacità di lasciarsi ispirare dalle opere degli altri per esprimere meglio i propri pensieri: è questo il significato di _borgen_ (prendere in prestito) e _abtragen_ (ripagare il debito) in F 7, significa far _fruttare_ le letture fatte con la creazione di nuovi pensieri e riflessioni, significa _"sbirciare cose nuove attraverso vecchi buchi di serratura"_ (F 879).

## Illuminazioni sulla teoria del sogno

Di grande interesse sono anche le osservazioni di Lichtenberg sul tema del sogno, di cui troviamo 50 annotazioni negli _Zibaldoni_, nonché alcuni contributi pubblicati sul _Calendario Tascabile di Gottinga_, come il resoconto "Daß du auf dem Blocksberge wärst. Ein Traum wie viele Träume” (1778-1799, Che il diavolo ti porti. Un sogno come tanti), “Nachricht von einer Walrat-Fabrik” (Messaggio da una fabbrica di spermaceti) ed “Einige Betrachtungen über vorstehenden Aufsatz, nebst einem Traum” (1794, Alcune riflessioni riguardo al saggio sopracitato, insieme a un sogno). Il contesto storico-culturale nel quale si svilupparono queste idee era una Gottinga coinvolta nello sviluppo di nuove discipline, fermento che investì tutti gli intellettuali della Georg-August Universität.

La tradizione della satira onirica, cui palesemente si rifà il resoconto di Lichtenberg “Daß du auf dem Blocksberge wärst”, fu sviluppata in _Sogni_ (1754) del medico e filosofo Johann G. Krüger (1715-1759) &mdash; uno dei principali esponenti della scuola degli _Psychomediziner_ hallensi[^5] &mdash; e si tratta di una raccolta di sogni scritta parallelamente alle sue ricerche scientifiche. Nel 1756 Krüger pubblicò il saggio di psicologia sperimentale _Versuch einer Experimentalseelenlehre_ (Tentativo di una teoria sperimentale dell'anima), il cui sesto capitolo è dedicato al tema della veglia, del sonno e dei sogni. Questo contributo di Krüger era un chiaro attacco alla _Psychologia rationalis_ (1734) di Christian Wolff (1679-1754), il quale riteneva che i sogni fossero _"uno stato di idee oscure"_.[^6]

I rapporti di Lichtenberg con i _Praeceptores Germaniae_ forniscono allora un buono spunto per comprendere l’origine delle annotazioni degli _Zibaldoni_. Lasciando sempre aperto il discorso, Lichtenberg non elaborò mai una vera e propria teoria dei sogni, ma era convinto che i sogni portassero a una conoscenza più profonda dell'animo umano (_"so per irrefutabile esperienza che i sogni conducono alla conoscenza di sé"_ [F 684]). Cercando di dare una spiegazione scientifica al sogno, Lichtenberg ipotizzò che quando si sogna, si esperisce una sorta di "dissociazione cerebrale":

>Spesso, troppo spesso perché possa essere un caso, in sogno vediamo sdoppiate le persone defunte (sappiamo cioè che sono morte, e tuttavia parliamo con loro come fossero vive). Ciò merita la nostra attenzione: penso dipenda dal fatto che il nostro cervello è doppio, simmetrico. (F 1159) ❞<br><br>❝ Che a uno (per lo meno a me) capiti così spesso di sognare di star parlando con un morto del morto stesso potrebbe dipendere dagli emisferi simili del cervello, come si vede doppio quando si preme con la mano un occhio. Nel sogno siamo folli, lo scettro manca, ho sognato spesso di star mangiando carne umana cotta. (F 607)

Secondo Lichtenberg, è prerogativa dei sogni il sottrarsi alla ragione e sconvolgere ciò che l’abitudine ci fa dare per scontato: nel sogno sul mangiare carne umana cotta, ad esempio, il cannibalismo diventa violenta irruzione del mondo sconosciuto, interiore, che è in noi. Sogni come questi erano persino in grado di mettere in dubbio la convinzione (dell'epoca) della superiorità della civiltà europea (_"spesso un sogno ci fa cambiare idea e consolida le nostre fondamenta morali più di qualunque teoria, che arriva al cuore per la via più lunga"_ [A 125]).

Lo scienziato di Gottinga era inoltre convinto che i sogni fossero rappresentativi di un allargamento della ragione, in quanto appartengono al dominio dell'**irrazionale**. Difatti, i sogni non sono che una diversa maniera di _ars combinatoria_, ovvero connettono pensieri e concetti attraverso le immagini. Secondo tale prospettiva, i sogni non sono allora _illogici_, bensì solo _sottratti_ alla ragione. Riferendosi al sognatore con la parola _Narr_ (folle) &mdash; e non _Verrückt_ (pazzo) &mdash; Lichtenberg non definisce il sognare come uno stato di confusione patologica, bensì comprende che la **follia** può persino aiutare a vedere le cose in modo più acuto, chiaro,[^7] giacché in sogno _"le idee sono a volte incomparabilmente più chiare di quelle che si hanno nello stato di veglia"_.[^8]

---

## Bibliografia

- Canetti, Elias (1973). _Die Provinz des Menschen. Aufzeichnungen 1942-1972_, München: Carl Hanser Verlag.
- Cantarutti, Giulia (2004). "Letture di Lichtenberg", in G. Ruozzi a cura di 2004: 78-105.
- Cantarutti, Giulia (2006). "I 'Sudelbücher' di Lichtenberg", in M. A. Rigoni a cura di 2006: 215-239.
- Cantarutti, Giulia (2012). "'Raccomando ancora una volta i sogni'. Le riflessioni di uno scienziato nella Gottinga settecentesca", _Intersezioni_, 1: 49-71.
- Helmich, Werner (2006). "L'aforisma come genere letterario", in M. A. Rigoni a cura di 2006: 19-50.
- Kraus, Karl (1972). _Detti e contraddetti_, a cura di R. Calasso, Milano: Adelphi.
- Lichtenberg, Georg C. (2017). _Sudelbücher_, herausgegeben von F. H. Mautner, I, Frankfurt am Main und Leipzig: Insel Verlag.
- Lichtenberg, Georg C. (2002). _Zibaldone segreto_, a cura di F. Farina, Milano: Edizioni Virgilio.
- Muratori, Ludwig A. (1785). _Über die Einbildungskraft des Menschen_, herausgegeben von G. H. Richerz, Leipzig: Weygand.
- Niggl, Günter (1981). "Probleme literarischer Zweckformen", _Internationales Archiv für Sozialgeschichte der deutschen Literatur_, 1, VI: 1-18.
- Promies, Wolfgang (1992). _Schriften und Briefe. Kommentarband zu Band I und II_, München: Carl Hanser Verlag.
- Richerz, Georg H. (1785). "Zusätze des Herausgebers zum fünften und sechsten Kapitel", in L. A. Muratori 1785, I: 231-301.
- Rigoni, Mario A. a cura di (2006). _La felicità breve. Contributi alla teoria e alla storia dell'aforisma_, Venezia: Marsilio Editori.
- Ruozzi, Gino a cura di (2004). _Teoria e storia dell'aforisma_, Milano: Mondadori.

## Sitografia

- Aforismario (2009). "<a href="https://www.aforismario.eu/2019/03/definizione-di-aforisma.html" target="_blank">Definizione e caratteristiche dell'aforisma</a>".

---

## Note

[^1]: Come leggiamo nella _Divina Commedia_ (_Paradiso_ XI, v. 4), Dante utilizza la parola "amforismi" per indicare gli studi medici.
[^2]: V. Promies 1992.
[^3]: Un antico formato dei libri, ottenuto piegando tre volte un foglio intero.
[^4]: Passi scelti di un’opera, o delle varie opere di un autore.
[^5]: Medici allievi del luminare Albrecht von Haller (1708-1777), il quale studiò le proprietà del sistema nervoso e muscolare.
[^6]: V. Richerz 1785, I.
[^7]: A differenza della pazzia, la follia continua a intrattenere un rapporto con la realtà e quindi con la verità. Pensate ad esempio alla lucida follia del protagonista dell'_Enrico IV_ (1921) di Pirandello, o dell'_Amleto_ (1600-1602) di Shakespeare.
[^8]: V. Richerz 1785, I.