---
title: "Il tema del corpo nella letteratura tedesca. Una prospettiva neuroermeneutica"
date: 2024-04-20T16:35:51+02:00
featured: false
description: "Come dovrebbe essere condotta un'analisi neuroermeneutica senza cadere nella tentazione di utilizzare metodi tradizionali? In questo articolo si propone un esempio di analisi delle opere Pentesilea, La metamorfosi e Il piccolo signor Friedemann, attraverso una prospettiva orientata al lettore."
tags:
- neuroermeneutica
- letteratura tedesca
- approccio neuroermeneutico
- corpo femminile
toc: true
image: "/img/articoli/petto.jpg"
imageCaption: "Foto di <a href='https://unsplash.com/it/@struvictoryart' target='_blank'>Victoria Strukovskaya</a>"
---

---

## Introduzione

In <a href='https://volupia.it/articoli/la-neuroermeneutica-un-approccio-innovativo-alla-letteratura-tra-scienze-umanistiche-e-neuroscienze/' target='_blank'>questo articolo</a> si è parlato della **neuroermeneutica** e dell'innovazione che essa può portare nell'ambito dell'interpretazione letteraria. Probabilmente chi legge si starà chiedendo come dovrebbe essere condotta un'analisi neuroermeneutica senza cadere nella tentazione &mdash; e nei pericoli &mdash; di utilizzare metodi tradizionali. I metodi tradizionali comprendono, ad esempio, le **analisi decostruzioniste** che studiano le opere scomponendole nei loro elementi formali e storico-culturali. In termini di interpretazione, quest'ultimo tipo di approccio è orientato al _source text_, alle intenzioni dell'autore e all’interpretazione dello specialista, escludendo pertanto dall'analisi la ricezione dell'opera; inoltre, qualora essa venisse presa in considerazione nell'analisi, verrebbe tuttavia inserita nel contesto storico-letterario dell'opera (perché ebbe successo? Perché non venne apprezzata?).

Secondo il critico letterario Jean Starobinski (1970), ogni metodo ha in sé dei pericoli in quanto si ha la tendenza a trasformare l'analisi in un'operazione compiuta, chiusa e sterile. Tali pericoli potrebbero però essere superati qualora l'analisi proposta si configurasse come "momento" incompiuto, quindi aperta a nuove possibilità di interpretazione. Il cosiddetto "itinerario critico" (_trajet critique_) andrebbe dunque ripercorso e ampliato ogni volta, da ogni lettore. In questo articolo si propone allora un esempio di come potrebbe essere condotta un'**analisi neuroermeneutica** sul tema del **corpo** nella letteratura tedesca, attraverso una prospettiva orientata al **lettore** che si interroga sulla **ricezione** contemporanea delle opere in questione.

## Riflessione preliminare sul fare critica letteraria

Considerato che quello neuroermeneutico non è che uno dei tanti approcci a disposizione dello studioso di letteratura, prima di apprestarci all'analisi sarebbe opportuno riflettere brevemente sul **compito della critica letteraria**. In un noto saggio del 1894 Benedetto Croce esplora i vari percorsi a disposizione del critico letterario, chiedendosi cosa significhi fare critica. La critica analizza elementi come la biografia dell'autore, la storia dell'opera e il contesto storico, con lo scopo di ricostruire il ritratto psicologico dell'autore. Tuttavia &mdash; prosegue Croce &mdash; sottomettere il giudizio estetico di un'opera letteraria alla storia significa negare l'**indipendenza dell'arte**. Inoltre, riflettere sul rapporto tra storia e opera letteraria ci fa entrare in un circolo vizioso dal quale è difficile uscire:

{{< mquote content="Il sottomettere la storia dell’opera alla valutazione estetica è negare l’indipendenza e l’importanza intrinseca della storia; il sottomettere la valutazione estetica alla considerazione storica significa far della prima un accessorio inutile della seconda. Il sottomettere la biografia dell’autore alla spiegazione storica dell’opera significa annullare l’importanza della biografia; la quale se, per un certo rispetto, serve alla spiegazione dell’opera, in sé stessa non vuol essere altro che la storia dello svolgersi di una data personalità."
translationAuthor="Croce 1895: 17."
>}}

Infine, è interessante soffermarci su un ultimo aspetto della riflessione crociana: quando studiamo le cause e le condizioni che hanno portato alla genesi di un'opera, per quanto l'opera possa essere scadente o persino brutta, essa apparirà sempre bella ai nostri occhi. Che cosa è successo allora? È successo che l'opera ha smesso di essere espressione estetica di un qualcosa che interessi il nostro animo ed è diventata **espressione estetica della storia**. In realtà &mdash; continua Croce &mdash; non stiamo che dando un giudizio estetico al _nostro_ lavoro di critica e non più all'opera in sé!

Starobinski affronta invece il dilemma critico tra **storicizzazione** e **attualizzazione**. Come confrontarsi con quella distanza che intercorre tra una società e il pensiero? E come confrontarsi, invece, con la prossimità di un pensiero del passato al mondo contemporaneo? Il critico svizzero suggerisce di entrare nel **circolo ermeneutico**, il quale amplia l'interpretazione all'infinito. L'**ermeneutica** concepisce l'incontro tra testo e lettore in termini di una circolarità tra le **conoscenze pregresse** di chi legge e la **nuova conoscenza** trasmessa dal testo; la comprensione dell'opera letteraria si realizzerebbe allora come **partecipazione a un senso comune**.

George Lakoff & Mark Johnson (1980) propongono invece un'indagine immanente al testo che ne evidenzia le peculiarità formali, stilistiche e retoriche. La **poetica cognitiva** adotta una prospettiva che unisce studi letterari e scienze cognitive, e che molto deve alla **linguistica cognitiva** di Ronald Langacker (1991). Tale approccio è stato tuttavia criticato perché richiama troppo la teoria formalista dello **straniamento**, che ben si presta alla letteratura contemporanea ma ha scarso riscontro nella letteratura classica e preromantica.

Infine Renata Gambino & Grazia Pulvirenti (2018) propongono un approccio volto a risolvere le problematiche scaturite dalle indagini sia della poetica cognitiva, sia dell'ermeneutica. Partendo dalla dinamica del circolo ermeneutico, la <a href='https://volupia.it/articoli/la-neuroermeneutica-un-approccio-innovativo-alla-letteratura-tra-scienze-umanistiche-e-neuroscienze/' target='_blank'>neuroermeneutica</a> ambisce a cogliere le interazioni tra componenti filologiche, storico-culturali, antropologiche e fisiologiche per ricostruire i **fenomeni mentali** stimolati nel lettore da un testo. Quello neuroermeneutico è un **approccio costruttivista** che parte dal presupposto che il senso di un testo letterario debba essere sempre _costruito_ dai lettori. La neuroermeneutica esplora allora quei processi della mente umana che interessano la creazione dei significati, l'interazione con l'ambiente e l'elaborazione della propria esperienza del mondo. In questo modo l'analisi si aprirebbe a una **possibilità infinita di interpretazione**, in quanto le convenzioni di lettura sarebbero influenzate dal contesto storico, culturale e linguistico.

## L'importanza del tema del corpo

Nella prospettiva transdisciplinare della neuroermeneutica l'atto percettivo e la conoscenza sono concepiti come **esperienze incarnate**, ovvero come processi dinamici che permettono all'individuo di comunicare con il mondo attraverso il suo corpo. Il termine _embodied cognition_ si riferisce ai processi cognitivi che mettono in relazione mente, corpo e ambiente nell'unità definita _embodied mind_, cioè l'**intelletto incarnato**. La realtà non verrebbe allora colta come dato oggettivo, ma sarebbe il risultato di una **percezione soggettiva** che dipende dal soggetto: il **percettore** allora costruirebbe la realtà attraverso un atto di **autopoiesi**, ovvero secondo un _pattern_ scaturito dalla sua **specifica identità**, influenzato dal contesto storico-culturale e biologico.

La **realtà** non viene intesa come mera rappresentazione del mondo circostante, bensì come **enazione**, ovvero come delle azioni eseguite in relazione all'ambiente circostante e alle capacità del corpo umano. Il **corpo** è il mezzo tramite il quale è possibile vivere nel mondo e creare legami all’interno di una società. L’**identità di genere**, inoltre, gioca un ruolo centrale nella costruzione dell’**identità dell’individuo** e del **rapporto con il proprio corpo**; essa, difatti, ricopre un ruolo importantissimo nell’atto della lettura e dell’interpretazione del testo. Proprio per questo motivo è importante studiare il **tema del corpo** nella letteratura. 

Grazie all'approccio neuroermeneutico diverrebbe possibile condurre ricerche sul modo in cui la mente di lettrici e lettori viene stimolata durante la lettura, sul modo in cui il **loro stesso corpo** ne influenzi il pensiero, l’immaginazione, il modo in cui essi creano significati; sarebbe possibile studiare la ricezione contemporanea delle opere del passato e &mdash; grazie ai moderni sistemi di rilevazione delle neuroscienze &mdash; le sensazioni che queste ultime stimolerebbero nei corpi di lettrici e lettori.

## Il corpo è il perno del mondo

Le moderne società multietniche e multiculturali hanno messo in crisi la letteratura tradizionalmente intesa, la quale vedeva le origini di un patrimonio universale nell’antica Grecia e nell’antica Roma. Le contestazioni iniziarono verso gli anni Sessanta e provenivano innanzitutto dagli Stati Uniti, nelle cui università la letteratura veniva studiata attraverso nuove **prospettive multiculturali e interdisciplinari**. Le contestazioni più aspre venivano dal **femminismo**, che, insieme ai **Gender Studies**, denunciava il dominio di una **visione maschile** della società che di conseguenze si rifletteva anche nel campo della letteratura. La letteratura iniziò allora a essere interpretata con una sensibilità nuova, attraverso una **prospettiva interdisciplinare** che applicava criteri e metodi spesso trascurati dagli studiosi di letteratura: quelli della psicologia, della storia, della filosofia, della sociologia.

In questo discorso accademico il **tema del corpo** &mdash; soprattutto quello femminile &mdash; aveva assunto grandissimo rilievo, divenendo il fulcro della lotta per le rivendicazioni femministe. Tale _body revival_ (Frank 1990) si poneva in contrapposizione a ciò che fino ad allora era stato l’atteggiamento della sociologia, la quale focalizzava i propri studi unicamente su strutture sociali e istituzioni; tale atteggiamento fu definito da David Morgan & Sue Scott (1993) _"anti-body bias"_. La **sociologia**, difatti, osservava gli individui come fossero **entità disincarnate**, piuttosto che esseri viventi fatti di carne e sangue: da sempre considerato dominio delle scienze biologiche, si riteneva che il **corpo** appartenesse alla **natura** e che, di conseguenza, dovesse essere necessariamente contrapposto alla cultura e alla società.

Ma se la sociologia veniva accusata di essere una **scienza disincarnata**, alla scienza &mdash; al contrario &mdash; era rimproverato di osservare il corpo come un **fenomeno unicamente biologico**. Attraverso lo sguardo oggettivo della scienza, il corpo è di fatto trasformato in un _“oggetto di questo mondo”_ (Galimberti 2021), che viene dissezionato e di cui vengono studiati separatamente organi e funzioni. Naturalmente il rigore scientifico impone di adottare la logica causa-effetto, pertanto le uniche relazioni biologiche che la scienza riconosce sono quelle chimico-fisiche. Ciononostante Umberto Galimberti ci ricorda:

>Il corpo è incompatibile con lo statuto dell’oggetto perché è costantemente percepito, mentre dall’oggetto posso anche distogliere l’attenzione; perché è sempre con me e mai, come l’oggetto, di fronte a me.[^1]

Nel suo saggio _Il corpo_ (2021) il filosofo lombardo opera difatti una distinzione tra il "corpo" (la fisicità che ci permette di essere-nel-mondo) e l'"organismo" (la fisicità studiata dalla scienza). Assolutizzando l’oggettività del fatto, studiando un _organismo_, la scienza annulla di fatto **il legame tra coscienza, corpo e mondo**, pur tuttavia &mdash; asserisce Jacques Monod (1970) &mdash; dietro a questa oggettività non si trova la realtà, bensì una **convenzionalità**.

Già all’inizio del Novecento il filosofo Maurice Merleau-Ponty definiva il corpo la base della nostra **relazione con il mondo** (_“il mio corpo è il perno del mondo”_),[^2] poiché è solo abitandolo con il corpo che si può conoscere il mondo e la giusta forma delle cose. Il corpo è difatti l’unico mezzo tramite il quale è possibile **essere-nel-mondo** e, inoltre, se riusciamo a concepire i concetti più astratti è perché li mettiamo in relazione al nostro corpo per congruenza, distanza, vicinanza, identificazione ecc. Tali paragoni vengono fatti principalmente per mezzo di **metafore** e categorie spaziali: per gli esseri umani **essere-nel-mondo** è dunque **avere un mondo in cui proiettarsi**.

{{< figure
    src="/img/articoli/leonardo.jpg"
    attr="_L'uomo vitruviano_ (1490) di Leonardo Da Vinci, conservato nelle Gallerie dell'Accademia, Venezia. Fonte: Wikimedia Commons."
>}}

---

## Il tema del corpo nella letteratura tedesca

La moderna teoria dell’_embodiment_ (Varela et al. 1991, 1994) era già stata intuita nella Germania del XVIII secolo nel contesto del dibattito in merito al **dualismo cartesiano**. Protagoniste dell’epoca erano le città di Berlino e Jena, dove gli intellettuali tedeschi collaboravano in nome di una “sinfilosofia” (Cometa 2009), dibattito filosofico sull’essenza dell’uomo, dell’arte e della natura caratterizzato da una prospettiva interdisciplinare (fisiologia, filosofia, storia, biologia ecc.). Molti studi erano incentrati sull’interazione tra mente (_Geist_) e corpo (_Körper_) attraverso la quale si voleva comprendere l’uomo nella sua integrità (_der ganze Mensch_).

In contrapposizione a quanto sostenuto da Cartesio, dagli scritti di Friedrich Schelling, Johann W. von Goethe, Albrecht von Haller, Johann G. Krüger e Johann W. Ritter apprendiamo che l'essenza della natura umana risiederebbe nella fusione tra _Geist_ e _Körper_, in quanto l’anima avrebbe il potere di rappresentare il mondo solo in relazione alla **posizione del corpo umano** (Baumgarten 1750). Nella lingua tedesca, inoltre, esistono due termini per indicare il corpo: _Körper_ è il corpo fisico e tangibile, mentre _Leib_ esprime un concetto di corpo più filosofico, come unità fisica, percettiva e spirituale. Il _Leib_, tuttavia, non è replica fedele del corpo fisico, ma è un’immagine dinamica composta dalle varie sensazioni del corpo, come ad esempio la vista: non a caso la parola _Gesicht_ (viso) ha a che fare con l’atto del vedere (< ted. _Sicht_ [vista] < ted. _sehen_ [vedere]; viso < lat. _visus_ [vista] < lat. _videre_ [vedere]).

### _La metamorfosi_ (1915) di Franz Kafka

Autore di interesse per la tematica del corpo nella letteratura tedesca è Franz Kafka (1883-1924), autore del racconto grottesco _La metamorfosi_ (1915). Questo racconto è interessante ai fini di una ricerca orientata ai lettori perché cerca di farli immedesimare in uno scarafaggio: una mattina Gregor Samsa si sveglia ed è diventato uno scarafaggio di dimensioni umane, ma la cosa più sorprendente è che egli accetta la sua nuova condizione senza esserne turbato, _come se si fosse sempre sentito uno scarafaggio._

> Quando Gregor Samsa si risvegliò una mattina da sogni tormentosi si ritrovò nel suo letto trasformato in un **insetto gigantesco**. Giaceva sulla **schiena dura come una corazza** e sollevando un poco il capo poteva vedere la sua **pancia convessa, color marrone, suddivisa in grosse scaglie ricurve;** sulla cima la coperta, pronta a scivolar via, si reggeva appena. Le sue **numerose zampe, pietosamente esili se paragonate alle sue dimensioni,** gli **tremolavano disperate** davanti agli occhi. [...] Dapprima voleva uscir dal letto con la parte inferiore del corpo, ma questa parte che egli stesso del resto non aveva ancor veduto e **di cui non si faceva un’immagine esatta,** si dimostrò troppo difficile a smuovere.

Come teorizzato da Lakoff & Johnson (1980), l’esperienza umana è strettamente legata alla produzione di **metafore**, le quali sono a loro volta radicate nell’esperienza del corpo e ne creano l’immagine: per questo motivo ci si può sentire “leggeri”, “a pezzi” o “degli scarafaggi”, perché si sta descrivendo un modo di essere in rapporto al corpo, proprio e degli altri. I **rapporti con gli altri** sono infatti decisivi per la formazione della propria **immagine corporea**, la quale subisce delle modificazioni a seconda del fatto che ci si senta accolti o respinti.

### _Penthesilea_ (1808) di Heinrich von Kleist

In un contesto di studi illuminati e ricerca dell’armonia fisica e spirituale dell’uomo, nella Germania tra la fine del XVIII e l'inizio del XIX secolo spiccava tuttavia per contrasto _“l’antagonista naturale del classicismo weimariano”_ (Cometa 2015: 165) Heinrich von Kleist (1777-1811), _“simbolo del demonismo autolesionista tedesco”_ (ivi). Interessata ai fenomeni più oscuri della natura e della psiche, ed espressione di un romantico e morboso “wertherismo”, la poetica di Kleist ricercava la **spontaneità del sentire soggettivo** in contrasto con gli obblighi morali imposti dalla società, mettendo in scena veri e propri _Körperdramen_ al cui centro veniva posto il corpo dei personaggi, come la virilità incerta di Anfitrione, la femminilità feroce di Pentesilea o la castità di Eve.

_Penthesilea_ (1808), ad esempio, mette in scena il lato più oscuro e violento del dionisiaco greco. In questa tragedia l’espressione del dionisiaco è feroce e terrificante, come ben dimostra, nella scena finale, la terribile rima tra _Küsse_ (baci) e _Bisse_ (morsi): inebriata dalla frenesia del combattimento finale, come fosse una fiera, Pentesilea arriva a dilaniare con i denti il corpo dell’amato Achille.

Qui di seguito si propongono alcuni estratti in cui è possibile notare il modo in cui le **metafore del corpo** cambiano in base al personaggio in scena.

**Il corpo di Pentesilea dal punto di vista dell'amazzone:**
>Lasciate che posi il suo piede d’acciaio – a me sta bene – su
questa nuca. A che scopo dovrebbero due guance, **due guance
fiorite come le mie,** distinguersi più a lungo dal **fango** da cui sono nate? Lasciate che coi cavalli **mi trascini a casa riversa,** e che **questo corpo, pieno di vita e di freschezza,** vergognosamente sia abbandonato in questi campi aperti; **lo doni ai cani, per il pasto mattutino,** lo offra alla disgustosa stirpe degli uccelli: meglio **essere polvere,** che **una donna che non seduce.**

**Il corpo di Pentesilea dal punto di vista di Ulisse e Diomede:**
>La regina, **senza più colore,** per due minuti lascia cadere le braccia: e poi, scuotendo i riccioli, **feroce**, sulle **guance riaccese di fiamma**, si rizza altissima sopra la groppa del cavallo, e gli affonda, **come un fulmine** strappato dal firmamento, la spada nella gola, così che lui, l’intruso, rotola giù ai piedi del divino figlio di Teti. […] Infatti **come la cagna**, liberata dal guinzaglio, si precipita **ululando** tra le corna del cervo (e il cacciatore, ansioso, la richiama, e la vuol far desistere; ma lei, **con i denti affondati nel collo** dello stupendo
animale, gli danza accanto su per le montagne.

**Il corpo di Achille dal punto di vista di Pentesilea:**
>E allora ti saluto, **fascino fresco della vita, giovane
dio dalle guance rosate!** […] Avanti, adesso, vergini
vittoriose, figlie di Marte, dalla testa ai piedi coperte
della polvere della battaglia, ciascuna avanti col
**giovinetto argivo** per la mano che prima l’ha
sopraffatto! […] Così tu adesso **non ti ribellerai** più
di quanto farebbe una **colomba,** se una bambina le
**stringesse i lacci intorno al collo**.

L’opera è intessuta sulla contrapposizione all'epoca anticonvenzionale tra **maschile e femminile**, in cui a Pentesilea vengono attribuiti dei caratteri maschili, mentre ad Achille dei caratteri femminili. In questa tragedia, inoltre, la regina delle amazzoni viene rappresentata come un **abominio**, rappresentante di un **titanismo femminile** che porta a orrore e morte, rientrando perfettamente in quella **visione maschile** che vede le donne come **l’altro** (_das Fremde_): irrazionale, misterioso, indisciplinato, capace di sfidare l’ordine sociale attraverso la seduzione, portando a nient’altro che lussuria, violenza e morte.

## La donna e l'estraneo

Come spiega Friedrich Schleiermacher (1838), l’atto dell’interpretazione non si esaurisce nel nesso fra parola e pensiero, ma richiede anche una _"conoscenza della totalità della dimensione storica”_ (Schleiermacher 1838: 1) in cui il linguaggio è situato, ovvero la conoscenza di un dato contesto culturale; ma siccome le _“differenze dello spirito umano sono determinate anche dalla natura fisica del corpo umano e dell’ambiente fisico terrestre”_ (ivi), le radici dell’ermeneutica non devono affondare solamente nell’etica, ma anche nella **fisica**, nell’unità della conoscenza. Come abbiamo visto, i _Gender Studies_ sono stati
cruciali per l’adozione, nell’ambito della letteratura, di nuove prospettive che tenessero in considerazione proprio quell’insieme di dinamiche extra-testuali essenziali per la comprensione di
un testo letterario.

Considerando la sfera della **corporeità** legata all’esperienza individuale e allo status sociale, Iris Young (1990) formula una teoria che scardina il potere dei gruppi dominanti e denuncia il fatto che i **privilegi** di questi ultimi sarebbero fondati sulle differenze e le disuguaglianze originate dall'avere differenti estrazioni sociali, _background_ etnici, locazioni geografiche, orientamenti sessuali, condizioni di salute ecc. Tale teoria pone il **corpo** quale strumento prediletto per comprendere come i **gruppi dominanti** &mdash; uomini bianchi, occidentali e borghesi &mdash; creino la categoria dell’**altro** (_das Fremde_) relegandovi certi gruppi sociali: donne, persone anziane, disabili, omosessuali, grasse, di colore e così via. Il corpo diventa allora **motivo di discriminazione**, la ragione per cui certi soggetti vengono definiti “brutti”, “ripugnanti”, “impuri”, “malati” o “deviati”: tale _“ridimensionamento estetico dei corpi”_ (_aesthetic scaling of bodies_) &mdash; come lo chiama Young &mdash; giustificherebbe l’inuguaglianza sociale e le gerarchie di potere, e lo si potrebbe
identificare quale perno su cui ruota l’**atto della dominazione**.

A partire dagli anni sessanta del Novecento, inoltre, è stato condotto un gran numero di ricerche empiriche sul modo in cui le donne facessero esperienza del proprio corpo e su come quest’ultimo fosse implicato nelle varie pratiche sociali e culturali. A livello teorico, Dorothy Smith (1990) ha provato a concettualizzare il ruolo, il corpo e la rappresentazione simbolica delle donne, rilevando come, in una società in cui sono istruite sull’**inferiorità del corpo femminile** (pensiamo ad esempio al tabù delle mestruazioni), le donne assumano spesso un atteggiamento che le porta a considerare i loro corpi come “manchevoli” e quindi come oggetti da “aggiustare”. Di conseguenza, la continua insoddisfazione le porterebbe a interagire con il proprio corpo come se fosse uno strumento da lavoro, uno strumento per _“fare la femminilità”_,[^3] ma sempre “estraneo” e “indomabile”.

L’identità di genere gioca infatti un ruolo centrale nella costruzione sia dell’**identità** dell’individuo, sia del suo rapporto con il corpo. La femminilità, così come la mascolinità, è espressa dal corpo in termini di dimensioni, forza, resistenza, flessibilità, ma anche per mezzo di abitudini assimilate con l’educazione (igiene, nutrizione, consumo, buon gusto, abbigliamento ecc…). Come sostengono molti studi, l’**identità personale** è in realtà frutto di un lungo processo d’interiorizzazione di esempi di “vera e giusta” femminilità o mascolinità provenienti dalla società in cui si vive, e poi rielaborati in versioni individuali grazie all’educazione famigliare, scolastica e all’esperienza personale.

### _Il piccolo signor Friedemann_ (1897) di Thomas Mann

_Il piccolo signor Friedemann_ è uno dei tanti esempi di testo a **focalizzazione maschile della narrazione**, in cui le donne sono viste come attrici esterne: belle, ma spesso crudeli. Quel che spesso manca alla letteratura prima della _Fin de siècle_ è proprio una **focalizzazione femminile** che non veda i personaggi femminili attraverso stereotipi di genere, quindi come emotivi, irrazionali, ingenui, crudeli, sensuali, di nervi fragili, approfittatori, angeli, madri o prostitute.

Il protagonista del racconto è un uomo appartenente all’alta borghesia, bene integrato nella società ma che al contempo suscita pietà e derisione a causa del suo grottesco aspetto fisico. Convinto di non potere mai trovare l’amore, Johannes dedica tutto se stesso al lavoro e al culto dell’arte, l'unica cosa in grado di fargli provare il vero piacere dei sensi.

{{< mquote content="Loro non sapevano che quello **storpio infelice**, che per via della sua **buffa importanza** marciava per la strada con un soprabito chiaro e un cilindro lucido – **in qualche strano modo era un po’ vanitoso** – amava con tenerezza la vita che lo attraversava placidamente, senza grandi emozioni, ma che, tuttavia, era piena di una tranquilla e **dolce felicità che sapeva di essersi costruito**."
translationAuthor="Traduzione di Alessandra Gallia"
>}}

Un giorno però Johannes incontra a Berlino Gerda von Rinnlingen, la moglie di un ufficiale, alla quale egli dà lezioni di musica. Gerda è una donna dalla bellezza “naturale”, ma è “glaciale” con il marito, “emotivamente malata” e con un comportamento da “maschiaccio”. I due provano una profonda connessione spirituale, ma quando Johannes sente di poterle confessare i propri sentimenti, Gerda lo respinge ridendogli in faccia: affranto dal dolore, egli si lascia annegare in un fiume.

{{< mquote content="'E avete creduto di essere felice?' Gli chiese. 'Ci ho provato' disse, ed ella rispose: 'Siete stato coraggioso'. Era passato un minuto. C'era solo il sommesso frinire dei grilli proveniente dagli alberi dietro di loro. 'Io un po' lo capisco cosa sia l'infelicità' disse ella infine. 'Certe notti d'estate in riva al fiume sono il miglior rimedio'. A queste parole egli non rispose ma indicò debolmente la riva opposta del fiume, che si stendeva placidamente nell'oscurità. 'Mi sono seduto lì di recente' disse soltanto. 'Quando siete venuto da me?' Chiese ella. Egli annuì. [...] Le toccò la mano, che era poggiata accanto a lui sulla panchina, con la sua e, mentre la stringeva, mentre le afferrava anche l'altra, mentre **quel piccolo uomo adulto, tremante, colto da spasmi, in ginocchio davanti a lei, premeva il suo viso contro il grembo di lei, con voce inumana, ansimante, balbettò:** 'Voi lo sapete già... Lasciate che io... Io non ce la faccio più... Dio mio... Dio mio...' Ella non fece resistenza, ma nemmeno si fletté su di lui. Sedeva con la schiena dritta, che si era leggermente ritratta da lui, gli occhi piccoli e ravvicinati, nei quali sembrava riflettersi il luccichio umido delle acque, guardavano fisso e tesi davanti a sé, oltre lui, in lontananza. Ma poi, **improvvisamente, con uno scatto, con una breve, fiera, spregevole risata, ella strappò le sue mani dalla presa di quelle dita eccitate, lo afferrò per un braccio e lo scagliò da parte, a terra.**"
translationAuthor="Traduzione di Alessandra Gallia"
>}}

In questo racconto è interessante la **focalizzazione interna** della narrazione, dove il narratore, seppur esterno, racconta le vicende dal punto di vista di Johannes e fa empatizzare il lettore con il protagonista: conosciamo quel che accade nella storia attraverso le sensazioni del corpo di Johannes; di Gerda, invece, non conosciamo pensieri e sentimenti, ma solo le impressioni che il suo comportamento suscita nel protagonista e, nel finale, diventa quasi inevitabile vederla come una persona crudele e insensibile, empatizzando con il povero protagonista e confermando le dicerie che circolavano sul conto di lei. Nonostante le anime di Johannes e Gerda fossero affini, i loro corpi sono rimasti distanti e si sono ingannati a vicenda.

In questo racconto al lettore è tuttavia affidato un importante **compito morale**: poiché il narratore è di parte, ma soprattutto è ironico, il lettore ha il compito di raccogliere gli indizi disseminati nella narrazione per costruire nella propria mente il comportamento di Johannes "visto da fuori". Soprattutto nella scena finale riportata sopra: Gerda è stata davvero così "spregevole" come dice il narratore, o è stato Johannes ad aver assunto un comportamento inappropriato nei suoi confronti?

## L'analisi neuroermeneutica

Visti gli esempi riportati sopra, un'analisi neuroermeneutica potrebbe essere condotta preparando dei questionari da somministrare a una platea. Nel questionario si potrebbero riportare gli estratti di cui sopra e chiedere alle lettrici e ai lettori di rispondere ad alcune domande sulle sensazioni corporee e le emozioni provate durante la lettura, nonché sulle difficoltà incontrate (se presenti). Grazie ai dati raccolti sarebbe interessante analizzare quali elementi nel _foregrounding_ suscitano determinate emozioni (allegria, soggezione, disapprovazione, disgusto, paura, rabbia ecc.) per cercare di capire quale livello (fonologico, semantico, lessicale ecc.) è presumibilmente il responsabile di una determinata risposta emotiva.

Si potrebbe inoltre chiedere ai partecipanti di fornire le descrizioni delle loro **image schemata**, cioè le immagini che vedono nella loro immaginazione mentre leggono, così come un'opinione sulle azioni/emozioni dei personaggi. Sarà poi compito del ricercatore o della ricercatrice analizzare i fattori che hanno facilitato l'immersione nella dimensione diegetica (_foregrounding_ e _backgrounding_) e l'**empatia**, così come i fattori che invece le hanno ostacolate. I risultati, infine, dovrebbero essere suddivisi in base a identità di genere, età ed etnia dei lettori, con lo scopo di individuare punti in comune e in contrasto.

## Conclusioni

Come abbiamo visto, la neuroermeneutica da un lato interpreta il testo letterario quale dispositivo della **conoscenza umana** ascritto a una **dimensione storico-culturale**, mentre dall'altro studia l'interazione lettore-testo attraverso una **prospettiva incarnata** che tiene in considerazione sia il **background** dei lettori (memorie, cultura d'appartenenza, identità di genere ecc.), sia il coinvolgimento della loro **sfera fisica, emotiva e immaginativa**. In questo modo i lettori vengono concepiti come **riceventi attivi** in grado di mettere in discussione l'immanenza del testo; pertanto oltre a studiare il contesto dell'opera, la neuroermeneutica studia il modo in cui l'opera del passato viene letta e interpretata dai lettori del mondo contemporaneo, cercando di cogliere quello scarto nella ricezione dell'opera tra passato e presente.

---

## Bibliografia

- Baumgarten, Alexander G. (1750). _Metaphysica_, Magdeburg: Impensis Carol Herman Hemmerde.
- Capucci, Luigi (1994). _Il corpo tecnologico. L’influenza delle tecnologie sul corpo e le sue facoltà_, Bologna: Baskerville.
- Cometa, Michele (2009). _L’età classico-romantica_, Roma: Laterza.
- Cometa, Michele (2015). _L’età di Goethe_, Roma: Carrocci.
- Croce, Benedetto (1895). _La critica letteraria. Questioni teoriche_, Roma: Loescher.
- Davis, Kathy, edited by (1997). _Embodied Practices. Feminist Perspectives on the Body_, London: SAGE Publication Inc.
- Davis, Kathy (1997). “Embody-ing Theory. Beyond Modernist and Postmodernist Readings of the Body”, in Davis eds 1997: 1-23.
- Frank, Arthur W. (1990). "Bringing Bodies Back In: A Decade Review", _Theory, Culture and Society_, 7: 131-162.
- Galimberti, Umberto (2021). _Il corpo_, Milano: Feltrinelli.
- Gambino, Renata, Pulvirenti, Grazia (2018). _Storie menti mondi. Approccio neuroermeneutico alla letteratura_, Udine e Milano: Mimesis.
- Kafka, Franz (2013). _La metamorfosi_, tr. it. L. Coppé, Roma: Newton Compton.
- Kleist, Heinrich von (1989), _Pentesilea_, tr. it. E. Filippini, Torino: Einaudi.
- Lakoff, George, Johnson, Mark (1980), _Metaphors We Live by_, Chicago: University of Chicago Press.
- Langacker, Ronald W. (1991). _Concept, Image, and Symbol: The Cognitive Basis of Grammar_, Berlin and New York: Mouton de Gruyter.
- Mann, Thomas (1966). _Der kleine Herr Friedemann_, Frankfurt a.M: S. Fischer.
- Merleau-Ponty, Maurice (1972), _Fenomenologia della percezione_, tr. it. A. Bonomi, Milano: il Saggiatore.
- Mirandola, Giorgio (1972). "Il metodo critico di Jean Starobinski", _Lettere italiane_, 2, XXIV: 232-251.
- Monod, Jacques (1970), _Il caso e la necessità_, tr. it. A. Busi, Milano: Mondadori.
- Morgan, David, Scott, Sue (1993), “Bodies in a Social Landscape”, in Scott and Morgan eds 1993: 1-21.
- Omel’čenko, Elena (2013). "Molodežnoe telo v seksual’no-gendernom izmerenii: zony molčanija vs otkrovenija", in Omel’čenka i Nartovoj pod redakcej 2013: 115-150.
- Omel’čenko, Elena, Nartovaja, Nadija pod redakcej (2013). _Pro Telo. Molodežnyj kontekst_, Sankt-Peterburg: Aletejja.
- Rossi, Federico (2011). "La poetica cognitiva: pro e contro", _Italianistica: rivista di letteratura italiana_, 3, XL: 47-60.
- Schleiermacher, Friedrich (1838), _Hermeneutik und Kritik_, in Id., Sämmtliche Werke, Berlin: Reimer: 5-262.
- Scott, Sue, Morgan, David edited by (1993). _Body Matters_, London and Washington D. C: The Falmer Press.
- Smith, Dorothy E. (1990). _Texts, Facts and Femininity. Exploring the Relations of Ruling_, London and New York: Routledge.
- Starobinski, Jean (1970). _La relation critique_, Paris: Gallimard.
- Varela, Francisco J., Thompson, Evan, et al. edited by (1991), _The Embodied Mind. Cognitive Science and Human Experience_, Cambridge: MIT Press.
- Varela, Francisco J. (1994). “Il reincanto del concreto”, in Capucci a cura di 1994: 143-159.
- Young, Iris (1990), _Justice and the Politics of Difference_, Princeton: Princeton University Press.

## Sitografia

- Mazzaglia, Piera, Mignemi, Federica (2019). <a href='http://www.neurohumanitiestudies.eu/wp-content/uploads/2019/05/Emozioni-Mazzaglia-Mignemi.pptx' target='_blank'>"Emozioni"</a>.

---

## Note

[^1]: Cit. Galimberti 2021: 81.
[^2]: V. Merleau-Ponty 1972: 130.
[^3]: Si pensi ad esempio alle pratiche alimentari, di igiene femminile, di cura per la persona e della chirurgia estetica.